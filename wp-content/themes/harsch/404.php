<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package harsch
 */

get_header(); ?>

	
			<div class="spacing__large theme__lightGrey"></div>
			<section class="error-404 not-found theme__lightGrey">
				<center>
				
				<h2 class="page-title"><?php _e( "Oups ! Cette page n'existe pas.", 'harsch' ); ?></h2>
				
				<div class="page-content">
					<p><?php _e( 'Pour toute question, ', 'harsch' ); ?><a href="<?php echo home_url();?>/contact/#form "><?php _e( 'contactez-nous.', 'harsch' ); ?></a></p>
				</div>

				</center>
				<div class="spacing__large theme__lightGrey"></div>
			</section>
			

		
<?php
get_footer();
