<?php
if(get_field('first_icon_text','option')){
	$first_icon_text=get_field('first_icon_text','option');
	$first_icon_url=get_field('first_icon_url','option');
}
if(get_field('second_icon_text','option')){
	$second_icon_text=get_field('second_icon_text','option');
	$second_icon_url=get_field('second_icon_url','option');
}
if(get_field('third_icon_text','option')){
	$third_icon_text=get_field('third_icon_text','option');
	$third_icon_url=get_field('third_icon_url','option');
}

?>