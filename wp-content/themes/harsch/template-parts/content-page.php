<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package harsch
 */

?>
<section class="intro">
    <div class="container">
        <h2 class="text-center"><?php the_title(); ?></h2>
        
            <p>
                <?php
			the_content();

				?>
            </p>
            
        </div>
    </div>    
</section>
