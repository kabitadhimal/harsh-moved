(function(){

	tinymce.PluginManager.add('shortcodebutton',function(editor,url){

		var shortcodesValues = [];

		editor.addButton('shortcodebutton',{
			text:'Insert Shortcodes',
			title:'Insert Shortcodes',
			icon:false,
			type:'menubutton',
			menu:[
					{
						text:'Shortbutton',
						value:'shortbutton',
						onclick:function (){
									insertShortcodeContent(editor,this);
						}
					},

					{
						text:'Highlighttext',
						value:'highlighttext',
						onclick:function (){
									insertShortcodeContent(editor,this);
						}
					},
					{
						text:'Quote',
						value:'quote',
						onclick:function (){
									insertShortcodeContent(editor,this);
						}
					},
					{
						text:'Columns',
						value:'columns',
	
						menu:[
							{
								text:'Column 1/2',
								value:'column_1_2',
								onclick:function (){
									insertShortcodeContent(editor,this);
								}
							},
							
							{
								text:'Column 1/4',
								value:'column_1_4',
								onclick:function (){
									insertShortcodeContent(editor,this);
								}
							}
						]
					},

					{
						text:'Linebreak',
						value:'linebreak',
						onclick:function (){
									insertShortcodeContent(editor,this);
						}
					},
					{
						text:'Largespace',
						value:'largespace',
						onclick:function (){
									insertShortcodeContent(editor,this);
						}
					},
					{
						text:'Smallspace',
						value:'smallspace',
						onclick:function (){
									insertShortcodeContent(editor,this);
						}
					},
					{
						text:'Mediumspace',
						value:'mediumspace',
						onclick:function (){
									insertShortcodeContent(editor,this);
						}
					},
					

    			]
		})
	})

})();

function insertShortcodeContent(editor,th){
	shortcode_type = th.value();

	if(shortcode_type == 'shortbutton')
	{
		var html = "[shortbutton url ='' id='' target='' color='' class='']text[/shortbutton]";
	}
	if(shortcode_type == 'highlighttext')
	{
		var html = "[highlighttext]hilighted text[/highlighttext]";
	}
	if(shortcode_type == 'quote')
	{
		var html = "[quote]Quote text[/quote]";
	}
	
	if(shortcode_type == 'column_1_2')
	{
    	html  = "[columns] [column 1/2][/column][column 1/2][/column][/columns]";
	}
	
	if(shortcode_type == 'column_1_4')
	{
    	html  = "[columns] [column 1/4 title=''][/column][column 1/4 title=''][/column][column 1/4 title=''][/column][column 1/4 title=''][/column][/columns]";
	}
	if(shortcode_type == 'linebreak')
	{
		var html = "[linebreak][/linebreak]";
	}
	if(shortcode_type == 'largespace')
	{
		var html = "[largespace][/largespace]";
	}
	if(shortcode_type == 'smallspace')
	{
		var html = "[smallspace][/smallspace]";
	}
	if(shortcode_type == 'mediumspace')
	{
		var html = "[mediumspace][/mediumspace]";
	}

	editor.insertContent(html);
} 