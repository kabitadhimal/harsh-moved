<?php

new Shortcode_Tinymce();
class Shortcode_Tinymce
{
    public function __construct()
    {
        add_action('admin_init', array($this, 'dnc_shortcode_button'));
        add_action('admin_footer', array($this, 'dnc_get_shortcodes'));
    }

    /**
     * Create a shortcode button for tinymce
     *
     * @return [type] [description]
     */
    public function dnc_shortcode_button()
    {
        if( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
        {
            add_filter( 'mce_external_plugins', array($this, 'dnc_add_buttons' ));
            add_filter( 'mce_buttons', array($this, 'dnc_register_buttons' ));
        }
    }

    /**
     * Add new Javascript to the plugin scrippt array
     *
     * @param  Array $plugin_array - Array of scripts
     *
     * @return Array
     */
    public function dnc_add_buttons( $plugin_array )
    {
       // $plugin_array['shortcodebutton'] = plugin_dir_url(__FILE__).'tinyshortcodes.js';
        $plugin_array['shortcodebutton'] = get_template_directory_uri().'/shortcodes/tinyshortcodes.js';
		return $plugin_array;
    }

    /**
     * Add new button to tinymce
     *
     * @param  Array $buttons - Array of buttons
     *
     * @return Array
     */
    public function dnc_register_buttons( $buttons )
    {
        array_push($buttons,'seperator','shortcodebutton');
		return $buttons;
    }

    /**
     * Add shortcode JS to the page
     *
     * @return HTML
     */
    public function dnc_get_shortcodes()
    {
        /*global $shortcode_tags;

        $your_text_here = __('Your text here','tiny_all');

        $shortcodes = array('squares_image'=>__('Squares image','tiny_all'),
                            'smart_squares_image'=>__('Smart squares image','tiny_all'),
                            'full_width_text'=>__('Full width text','tiny_all'),
                            'full_width_picture'=>__('Full width picture','tiny_all'),
                            'smart_full_width picture'=>__('Smart full width picture','tiny_all'),
                            'slider'=>__('Slider','tiny_all')  );
        echo '<script type="text/javascript">

        var shortcodes_button = new Array();';
        echo "your_text_here = '{$your_text_here}';";

        $count = 0;

        foreach($shortcodes as $tag => $code)
        {
            echo "shortcodes_button[{$count}] = '{$code}';";
            $count++;
        }

        echo '</script>';*/
    }
}




?>