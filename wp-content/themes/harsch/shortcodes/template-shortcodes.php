<?php
function harsch_button($atts, $content = null)
{
    extract(shortcode_atts(
        array(
            'url' => '',
            'target'=>'',
            'color'=>'',
            'id'=>'',
            'class' => ''
        ),
        $atts
    ));
    $output = '<a id="'.$id.'" href="'. $url .'" class="btn btn--main btn--main__'.$color.' '.$class.'" target="'.$target.'">'. do_shortcode($content) .'</a>';
    return $output;
}



function harsch_highlight($atts, $content = null)
{
    extract(shortcode_atts(
        array(
            'class' => '',
        ),
        $atts
    ));
    $output = '<span class="highlight">'. do_shortcode($content) .'</span>';
    return $output;
}


function harsch_quote($atts, $content = null)
{
   $output = '
       <div class="col">
        <div class="spacing__small"></div>
            <div class="quotation">
                <p class="para__large text-center">'. do_shortcode($content).'</p>
            </div>
        <div class="spacing__small"></div>
        </div>';
    return $output;
}

function columns_shortcode( $atts,$content) {

        $html = '<div class="col">'.do_shortcode( $content ).'</div>';

        //echo $html; die('die');
        return $html;
    }

function column_shortcode( $atts,$content) {
            if(!empty($atts))
            {

                if($atts[0] == '1/2'){
                    $html = '<div class="col col--2" ><p class="text-justify">'.do_shortcode(($content)).'</p></div>';
                }
               
                else if($atts[0] == '1/4'){
                    $h4_title='';
                    if($atts["title"]!=''){
                        $h4_title='<div class="small--title">'.$atts["title"].'</div>';
                    }
                    $html = '<div class="col col--4">'.$h4_title.'<p class="text-justify">'.do_shortcode(($content)).'</p></div>';
                }
            }

         return $html;
    }

function linebreak_shortcode( $atts,$content=null) {
        $html = '<br>';   
        return $html;
    }
function large_spacing_shortcode( $atts,$content=null) {
        $html = '<div class="spacing__large"></div>';   
        return $html;
    }
 function small_spacing_shortcode( $atts,$content=null) {
        $html = '<div class="spacing__small"></div>';   
        return $html;
    }
function medium_spacing_shortcode( $atts,$content=null) {
        $html = '<div class="spacing__medium"></div>';   
        return $html;
    }

function harsch_register_shortcodes()
{
   
    add_shortcode('shortbutton','harsch_button');

    add_shortcode('highlighttext','harsch_highlight');

    add_shortcode('quote','harsch_quote');   

    add_shortcode( 'columns', 'columns_shortcode' );

    add_shortcode( 'column', 'column_shortcode' );

    add_shortcode( 'linebreak', 'linebreak_shortcode' );
    add_shortcode( 'largespace', 'large_spacing_shortcode' );
    add_shortcode( 'smallspace', 'small_spacing_shortcode' );
    add_shortcode( 'mediumspace', 'medium_spacing_shortcode' );

}

add_action('init','harsch_register_shortcodes');

