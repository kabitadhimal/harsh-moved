<?php
/**
 * harsch functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package harsch
 */

define('CONTACT_LINK', icl_get_home_url().'contact/#bureaux');


if (!function_exists('debug')) {


    function debug($var = '', $die = true) {
        $array = debug_backtrace();
        echo '<br/>Debugging from ' . $array[0]['file'] . ' line: ' . $array[0]['line'];

        echo '<pre>';
        print_r($var);
        echo '</pre>';

        if ($die !== FALSE) {
            die();
        }
    }

}


if ( ! function_exists( 'harsch_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function harsch_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on harsch, use a find and replace
	 * to change 'harsch' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'harsch', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'harsch' ),
		'side' => esc_html__( 'Side Menu', 'harsch' ),
		
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'harsch_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'harsch_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function harsch_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'harsch_content_width', 640 );
}
add_action( 'after_setup_theme', 'harsch_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function harsch_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'harsch' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'harsch' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Social slider', 'harsch' ),
		'id'            => 'social-slider',
		'description'   => esc_html__( 'Add widgets here.', 'harsch' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Pre-Footer', 'harsch' ),
		'id'            => 'pre-footer',
		'description'   => esc_html__( 'Add widgets here.', 'harsch' ),
		'before_widget' => '<div id="%1$s" class="widget col-md-2-5th %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );
}
add_action( 'widgets_init', 'harsch_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function harsch_scripts() {
	wp_enqueue_style( 'harsch-style', get_stylesheet_uri() );
	wp_enqueue_style( 'harsch-style-font', get_template_directory_uri().'/css/font-awesome.min.css');
    
	wp_enqueue_style( 'harsch-style-css', get_template_directory_uri().'/css/style.css' );
    wp_enqueue_style( 'harsch-swiper-css', get_template_directory_uri().'/css/swiper.min.css' );
	
	wp_enqueue_script( 'harsch-navigation', get_template_directory_uri() . '/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js', true );
    wp_enqueue_style( 'harsch-fancy', get_template_directory_uri() . '/css/jquery.fancybox.css' );
    
    wp_enqueue_script( 'harsch-magnific', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', true );

	//wp_enqueue_script( 'harsch-bootstrap', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', true );
	//wp_enqueue_script( 'harsch-waypoint', get_template_directory_uri() . '/js/vendor/jquery.waypoints.js', true );
	//wp_enqueue_script( 'harsch-slick', get_template_directory_uri() . '/js/vendor/slick.min.js', true );
	//wp_enqueue_script( 'harsch-main', get_template_directory_uri() . '/js/main.js', true );
	
}
add_action( 'wp_enqueue_scripts', 'harsch_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
/**
* custom code begins here
*/


// to show buttons on editor at backend
require get_template_directory() . '/shortcodes/tinyshortcodes.php';
require get_template_directory() . '/shortcodes/template-shortcodes.php';

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_sub_page('Header');
    
    acf_add_options_sub_page('Footer');
}

//function to limit word in content
function content($my_content, $limit) {
    $content = explode(' ', $my_content, $limit);
    $cnt = count($content);
    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '';
    } else {
        $content = implode(" ", $content);
    }
    $content = preg_replace('/\[.+\]/', '', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = strip_tags($content);
    if ($cnt >= $limit) {
        return $content . '...';
    } else {
        return $content;
    }
}

/*add_action( 'after_setup_theme', 'my_rss_template' );
/**
 * Register custom RSS template.
 */
/*function my_rss_template() {
	add_feed( 'short', 'my_custom_rss_render' );
}

*
 * Custom RSS template callback.
 
function my_custom_rss_render() {
	get_template_part( 'feed', 'short' );
}
*/
//var_dump(get_post_meta(318));
function wp_get_menu_array($current_menu) {
 
    $array_menu = wp_get_nav_menu_items($current_menu);
      //echo '<pre>'; print_r($array_menu); echo '</pre>';
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {

            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($m->ID));   
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID']      =   $m->ID;
            $menu[$m->ID]['title']       =   $m->title;
            $menu[$m->ID]['url']         =   $m->url;
            $menu[$m->ID]['description'] =   $m->description;
            $menu[$m->ID]['classes']  = $m->classes;
            $menu[$m->ID]['classes']  = $m->classes;
            $menu[$m->ID]['imageurl']  = $image_url[0];
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {

        if ($m->menu_item_parent) {
            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($m->ID) , 'menu_image'); 
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID']       =   $m->ID;
            $submenu[$m->ID]['title']    =   $m->title;
            $submenu[$m->ID]['url']  =   $m->url;
            $submenu[$m->ID]['description'] = $m->description;
            $submenu[$m->ID]['classes']  = $m->classes;
            $submenu[$m->ID]['imageurl']  = $image_url[0];
         
         

            $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
        }
    }
    return $menu;
     
}




function acf_wysiwyg_remove_wpautop() {
    remove_filter('acf_the_content', 'wpautop' );
}
add_action('acf/init', 'acf_wysiwyg_remove_wpautop');


/* Change Excerpt length */
function get_excerpt($num){
$excerpt = get_the_content();
$excerpt = preg_replace(" ([.*?])",'',$excerpt);
$excerpt = strip_shortcodes($excerpt);
$excerpt = strip_tags($excerpt);
$excerpt = substr($excerpt, 0, $num);
$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
$excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));
$excerpt = $excerpt.'...';
return $excerpt;
}



// Insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
// Use 'mce_buttons' for button row #1, mce_buttons_3' for button row #3
add_filter('mce_buttons_2', 'my_mce_buttons_2');

function my_mce_before_init_insert_formats( $init_array ) {
    $style_formats = array(
        array(
            'title' => 'Accordian Class', // Title to show in dropdown
            'selector' => 'ul', // Element to add class to
            'classes' => 'general__list' // CSS class to add
        ),
        array(
            'title' => 'Fancy Box Class', // Title to show in dropdown
            'selector' => 'a', // Element to add class to
            'classes' => 'fancybox' // CSS class to add
        )
    );
    $init_array['style_formats'] = json_encode( $style_formats );
    return $init_array;
}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );


add_image_size('news_image',400,400,true);
add_image_size('menu_image',400,250,true);

add_action('init', function() {
    $url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/');

    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $uri_segments = array_filter($uri_segments);
    $uri_segments = array_values($uri_segments);


        //if(( $uri_segments[0] == 'harsch' && $uri_segments[1] == 'news' ||
        //    $uri_segments[0] == '~harsch' && $uri_segments[1] == 'news' ||
        //    $uri_segments[0] == 'news' ) && !empty($uri_segments[2]))

        $uri_segments0 = isset($uri_segments[0])?$uri_segments[0]:"";
        $uri_segments1 = isset($uri_segments[1])?$uri_segments[1]:"";
        $uri_segments2 = isset($uri_segments[2])?$uri_segments[2]:"";
        $uri_segments3 = isset($uri_segments[3])?$uri_segments[4]:"";

       if(( $uri_segments0 == 'harsch' && (isset($uri_segments1) && $uri_segments1== 'news') ||
            $uri_segments0 == '~harsch' && (isset($uri_segments1) && $uri_segments1== 'news') ||
            $uri_segments0 == 'news' ) && (isset($uri_segments1) && !empty($uri_segments2)))
        {
                $cat = $uri_segments[2];
                $cat_array = explode('=',$uri_segments[2]);

                $_GET['cat'] = $cat_array[1];
                if($cat_array[0] == 'cat')
                {
                    $load = locate_template('templates/blogs.php', true);
                    if ($load) {
                        exit(); // just exit if template was found and loaded
                    }
                }
        }
});

/** Add Page Number to Title and Meta Description for SEO **/
if ( ! function_exists( 'multipage_metadesc' ) ){
   function multipage_metadesc( $s ){
      global $page;
      $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
      ! empty ( $page ) && 1 < $page && $paged = $page;
      $paged > 1 && $s .= ' - ' . sprintf( __( 'Page %s' ), $paged );
      return $s;
   }
   add_filter( 'wpseo_metadesc', 'multipage_metadesc', 100, 1 );
}

if ( ! function_exists( 'multipage_title' ) ){
   function multipage_title( $title ){
      global $page;
      $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
      ! empty ( $page ) && 1 < $page && $paged = $page;
      $paged > 1 && $title .= ' - ' . sprintf( __( 'Page %s' ), $paged );
      return $title;
   }
   add_filter( 'wpseo_title', 'multipage_title', 100, 1 );
}

add_filter( 'wpcf7_validate_email*', 'custom_email_confirmation_validation_filter', 20, 2 );
 
function custom_email_confirmation_validation_filter( $result, $tag ) {
    if ( 'devisemail2' == $tag->name ) {
        $your_email = isset( $_POST['devisemail'] ) ? trim( $_POST['devisemail'] ) : '';
        $your_email_confirm = isset( $_POST['devisemail2'] ) ? trim( $_POST['devisemail2'] ) : '';
 
        if ( $your_email != $your_email_confirm ) {
            $result->invalidate( $tag, "Are you sure this is the correct address?" );
        }
    }
    return $result;
}

function action_wpcf7_mail_sent( $contact_form ) { 
    
	$id = $contact_form->id;
    if ( '10606' == $id || '10857' == $id ) {
		
		$submission = WPCF7_Submission::get_instance();

		if ( $submission ) {
			$posted_data = $submission->get_posted_data();
		}
		
		$instance_url = "https://sugarcrmp.harsch.ch/rest/v11";
		$username = "demandesinternet";
		$password = "Ah#uvY8I";

		//Login - POST /oauth2/token
		$auth_url = $instance_url . "/oauth2/token";

		$oauth2_token_arguments = array(
			"grant_type" => "password",
			//client id - default is sugar. 
			//It is recommended to create your own in Admin > OAuth Keys
			"client_id" => "sugar", 
			"client_secret" => "",
			"username" => $username,
			"password" => $password,
			//platform type - default is base.
			//It is recommend to change the platform to a custom name such as "custom_api" to avoid authentication conflicts.
			"platform" => "base" 
		);

		$auth_request = curl_init($auth_url);
		curl_setopt($auth_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
		curl_setopt($auth_request, CURLOPT_HEADER, false);
		curl_setopt($auth_request, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($auth_request, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($auth_request, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($auth_request, CURLOPT_HTTPHEADER, array(
			"Content-Type: application/json"
		));

		//convert arguments to json
		$json_arguments = json_encode($oauth2_token_arguments);
		curl_setopt($auth_request, CURLOPT_POSTFIELDS, $json_arguments);

		//execute request
		$oauth2_token_response = curl_exec($auth_request);

		//decode oauth2 response to get token
		$oauth2_token_response_obj = json_decode($oauth2_token_response);
		$oauth_token = $oauth2_token_response_obj->access_token;
		
		$devisfromcountry = $posted_data['devisfromcountry'];
		$devisfromcity = $posted_data['devisfromcity'];
		$devistocountry = $posted_data['devistocountry'];
		$devistocity = $posted_data['devistocity'];
		$devisdate = $posted_data['devisdate'];
		$deviscat = $posted_data['deviscat'];
		
		if ( $deviscat == 'Œuvre d’arts' || $deviscat == 'Œuvre d’arts' ) {
		    $deviscat = 'fineart';
		} else if ( $deviscat == 'Effet personnels' || $deviscat == 'Personal effects' ) {
		    $deviscat = 'demint';
		} else if ( $deviscat == 'Effets de bureau' || $deviscat == 'Office furniture' ) {
		    $deviscat = 'office';
		} else {
		    $deviscat = 'archive';
		}
		
		$devisvolume = $posted_data['devisvolume'];
		$devisprischarge = $posted_data['devisprischarge'];
		$devissociete = $posted_data['devissociete'];
		$deviscomment = $posted_data['deviscomment'];
		
		$leadtitle = $posted_data['devistitle'];
		$leadfirstname = $posted_data['devisprenom'];
		$leadlastname = $posted_data['devisnom'];
		$leademail = $posted_data['devisemail'];
		$leadphone = $posted_data['devisphone'];
		$devishear = $posted_data['devishear'];
		$devishearautre = $posted_data['devishearautre'];
		$devishear = $devishear . ' ' . $devishearautre;
		$deviscomment2 = $posted_data['deviscomment2'];
		
		$devisacceptance = $posted_data['devisacceptance'];
		
		$deviscomment = 'Date du déménagement: ' . $devisdate . ' ' . $deviscomment . ' ' . $deviscomment2;
		
		$devislang = $posted_data['lang'];
	
	//Create Records - POST /<module>
		$url = $instance_url . "/Leads";
		//Set up the Record details
		$record = array(
			'first_name' => $leadfirstname,
			'last_name' => $leadlastname,
			'email' => array(
				array(
					'email_address' => $leademail,
					'primary_address' => true
				)
			),
			'phone_work' => $leadphone,
			"lead_source" => "Web Site",
			"dotb_hh_from_country_c" => $devisfromcountry,
    		"dotb_hh_from_city_c" => $devisfromcity,
    		"dotb_hh_country_destination_c" => $devistocountry,
    		"dotb_hh_destination_city_c" => $devistocity,
    		"dotb_hh_department" => $deviscat,
    		"dotb_hh_arch_divers_vol" => $devisvolume,
    		"account_name" => $devissociete,
    		"description" => $deviscomment,
    		"salutation" => $leadtitle,
		    "lead_source_description" => $devishear,
		    "dotb_preferred_language" => $devislang,
		    "dotb_hh_newsletter_c" => $devisacceptance
		
		);

		$curl_request = curl_init($url);
		curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
		curl_setopt($curl_request, CURLOPT_HEADER, false);
		curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($curl_request, CURLOPT_HTTPHEADER, array(
			"Content-Type: application/json",
			"oauth-token: {$oauth_token}"
		));

		$json_arguments = json_encode($record);
		curl_setopt($curl_request, CURLOPT_POSTFIELDS, $json_arguments);
		$curl_response = curl_exec($curl_request);
		$createdRecord = json_decode($curl_response);

		//print_r($createdRecord);
		curl_close($curl_request);

	}
}; 
add_action( 'wpcf7_mail_sent', 'action_wpcf7_mail_sent', 10, 1 );

add_action( 'wp_footer', 'cf7_thank_you_redirect' );
function cf7_thank_you_redirect() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
	if ( '10606' == event.detail.contactFormId ) {
        //ga('send', 'event', 'Devis', 'Sent', '<?php echo get_permalink(); ?>');
		location = '<?php echo site_url(); ?>/merci-devis/';
    } else if ( '10857' == event.detail.contactFormId ) {
		location = '<?php echo site_url(); ?>/en/thank-you-quote/';
    }
}, false );
</script>
<?php }