(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {}; 

// library properties:
lib.properties = {
	width: 1662,
	height: 825,
	fps: 24,
	color: "#FFFFFF",
	webfonts: {},
	manifest: []
};



lib.webfontAvailable = function(family) { 
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.Bitmap1 = function() {
	this.spriteSheet = ss["map_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.map = function() {
	this.spriteSheet = ss["map_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.swiss = function() {
	this.spriteSheet = ss["map_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.map();
	this.instance.setTransform(-831,-412.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-831,-412.5,1662,825);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.map();
	this.instance.setTransform(-831,-412.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-831,-412.5,1662,825);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D5002A").s().p("AgWAXQgJgJAAgOQAAgNAJgJQALgKALAAQANAAAJAKQAKAJAAANQAAAOgKAJQgJAKgNAAQgLAAgLgKg");
	this.shape.setTransform(0,0,1.906,1.906);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.3,12.4,12.8);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#D5002A").ss(0.6).p("AAzAAQAAAWgPAPQgPAQgVAAQgUAAgPgQQgQgPAAgWQAAgVAQgPQAPgQAUAAQAVAAAPAQQAPAPAAAVg");
	this.shape.setTransform(0,0,1.906,1.906);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.9,-11.2,21.9,22.4);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#D5002A").ss(0.6).p("ABPAAQAAAigXAXQgYAYggAAQgfAAgYgYQgXgYAAghQAAggAXgYQAYgYAfAAQAgAAAYAYQAXAXAAAhg");
	this.shape.setTransform(0,0,1.906,1.906);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16.1,-16.5,32.3,33.1);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.swiss();
	this.instance.setTransform(-193,-133.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-193,-133.5,386,267);


(lib.marker = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D5002A").s().p("Ag0CYQhqiZgBg+QABhCAugvQAvgvBBAAQBBAAAvAvQAwAvAABCQgBAcgWAsQgbA8g6BTQgeArgWAdQgYgegcgqgAgqhvQgSASAAAZQAAAZASASQASASAYAAQAYAAATgRQASgTAAgZQAAgZgSgSQgTgSgYAAQgYAAgSASg");
	this.shape.setTransform(0,-22.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgqArQgSgSAAgZQAAgXASgTQASgSAYAAQAZAAASASQASATAAAXQAAAZgSASQgSASgZAAQgYAAgSgSg");
	this.shape_1.setTransform(0,-29.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag9A+QgagaABgkQgBgjAagZQAagaAjAAQAkAAAZAaQAaAZAAAjQAAAkgaAaQgZAZgkAAQgjAAgagZg");
	this.shape_2.setTransform(-0.2,-29.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16,-44.9,32,44.9);


(lib.spot_in = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy 3
	this.instance = new lib.Tween4("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.instance_1 = new lib.Tween5("synched",0);
	this.instance_1.setTransform(0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 1 copy
	this.instance_2 = new lib.Tween6("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16.1,-16.5,32.3,33.1);


(lib.spot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.spot_in();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(2).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.05,scaleY:1.05},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(2).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.04,scaleY:1.04},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.03,scaleY:1.03},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1.01,scaleY:1},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleY:1.01},0).wait(1).to({scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1.01},0).wait(2).to({scaleX:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.4,-15.8,30.9,31.7);


// stage content:



(lib.mapfin2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
		window.playAnimation = function(){
			this.play();
		}.bind(this);
	}
	this.frame_35 = function() {
		if(typeof(window.HarschMap)=='object'){
			window.HarschMap.showTitle();
		}
	}
	this.frame_91 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(35).call(this.frame_35).wait(56).call(this.frame_91).wait(1));

	// map_marker.svg
	this.instance = new lib.marker();
	this.instance.setTransform(818,278.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(86).to({_off:false},0).wait(6));

	// map_marker.svg
	this.instance_1 = new lib.marker();
	this.instance_1.setTransform(818,298.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(21).to({_off:false},0).wait(1).to({regY:-22.5,y:267.2},0).wait(1).to({y:259.6},0).wait(1).to({y:253.5},0).wait(1).to({y:249.4},0).wait(1).to({y:247.8},0).wait(1).to({y:249.2},0).wait(1).to({y:252.3},0).wait(1).to({y:255.9},0).wait(1).to({y:259},0).wait(1).to({y:260.8},0).wait(1).to({y:261.2},0).wait(1).to({y:260.1},0).wait(1).to({y:258.2},0).wait(1).to({y:256},0).wait(1).to({y:254.2},0).wait(1).to({y:253.2},0).wait(1).to({y:253.1},0).wait(1).to({y:253.8},0).wait(1).to({y:255},0).wait(1).to({y:256.3},0).wait(1).to({y:257.4},0).wait(1).to({y:257.9},0).wait(2).to({y:257.5},0).wait(1).to({y:256.7},0).wait(1).to({y:255.9},0).wait(1).to({y:255.3},0).wait(1).to({y:255},0).wait(2).to({y:255.4},0).wait(1).to({y:255.8},0).wait(1).to({y:256.3},0).wait(1).to({y:256.6},0).wait(1).to({y:256.8},0).wait(2).to({y:256.6},0).wait(1).to({y:256.3},0).wait(1).to({y:256},0).wait(1).to({y:255.8},0).wait(1).to({y:255.7},0).wait(2).to({y:255.9},0).wait(1).to({y:256},0).wait(1).to({y:256.2},0).wait(1).to({y:256.3},0).wait(1).to({y:256.4},0).wait(1).to({y:256.3},0).wait(2).to({y:256.1},0).wait(1).to({y:256},0).wait(5).to({y:256.1},0).wait(1).to({y:256.2},0).wait(5).to({y:256.1},0).wait(3).to({_off:true},1).wait(6));

	// spots copy 8
	this.instance_2 = new lib.spot();
	this.instance_2.setTransform(1586.2,159.4,0.405,0.404);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(57).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},14,cjs.Ease.get(1)).wait(21));

	// spots copy 7
	this.instance_3 = new lib.spot();
	this.instance_3.setTransform(1417.2,650,0.162,0.161);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(54).to({_off:false},0).to({scaleX:1,scaleY:1,y:649.9,alpha:1},14,cjs.Ease.get(1)).wait(24));

	// spots copy 6
	this.instance_4 = new lib.spot();
	this.instance_4.setTransform(1420.3,336.9,0.033,0.032);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(50).to({_off:false},0).to({scaleX:1,scaleY:1,x:1420.2,alpha:1},14,cjs.Ease.get(1)).wait(28));

	// spots copy
	this.instance_5 = new lib.spot();
	this.instance_5.setTransform(328.7,309.1,0.162,0.161);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(46).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},14,cjs.Ease.get(1)).wait(32));

	// spots
	this.instance_6 = new lib.spot();
	this.instance_6.setTransform(192.7,219.4,0.081,0.078);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(50).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},14,cjs.Ease.get(1)).wait(28));

	// spots copy 5
	this.instance_7 = new lib.spot();
	this.instance_7.setTransform(1152.8,273.7,0.162,0.161);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(38).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},14,cjs.Ease.get(1)).wait(40));

	// spots copy 2
	this.instance_8 = new lib.spot();
	this.instance_8.setTransform(477.6,634.2,0.066,0.064);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(46).to({_off:false},0).to({scaleX:1,scaleY:1,x:477.7,alpha:1},14,cjs.Ease.get(1)).wait(32));

	// spots copy 4
	this.instance_9 = new lib.spot();
	this.instance_9.setTransform(992.8,357.7,0.243,0.239);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(38).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},14,cjs.Ease.get(1)).wait(40));

	// spots copy 3
	this.instance_10 = new lib.spot();
	this.instance_10.setTransform(922.7,140.5,0.243,0.239);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(36).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},14,cjs.Ease.get(1)).wait(42));

	// Layer 5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_30 = new cjs.Graphics().p("EA/gAVdQgEgEAAgGQAAgGAEgEQAEgEAGAAQAFAAAEAEQAEAEAAAGQAAAGgEAEQgEAEgFgBQgGABgEgEg");
	var mask_graphics_31 = new cjs.Graphics().p("EA5XAWgQhjhjAAiNQAAiMBjhjQBkhjCMAAQCMAABkBjQBjBjAACMQAACNhjBjQhkBjiMAAQiMAAhkhjg");
	var mask_graphics_32 = new cjs.Graphics().p("EAzPAXjQjDjCAAkTQAAkTDDjDQDCjCETAAQEUAADCDCQDDDDAAETQAAETjDDCQjCDDkUAAQkTAAjCjDg");
	var mask_graphics_33 = new cjs.Graphics().p("EAtGAYnQkikiAAmaQAAmaEikiQEikfGaAAQGaAAEhEfQEiEiAAGaQAAGakiEiQkhEhmaAAQmaAAkikhg");
	var mask_graphics_34 = new cjs.Graphics().p("EAm9AZqQmBmBAAohQAAogGBl/QGBmBIhAAQIhAAGBGBQGBF/AAIgQAAIhmBGBQmBGBohAAQohAAmBmBg");
	var mask_graphics_35 = new cjs.Graphics().p("EAg1AatQnhngAAqnQAAqmHhngQHgngKnAAQKoAAHgHgQHhHgAAKmQAAKnnhHgQngHhqoAAQqnAAngnhg");
	var mask_graphics_36 = new cjs.Graphics().p("AasbxQpApAAAsuQAAssJApAQJAo/MuAAQMuAAJAI/QI/JAAAMsQAAMuo/JAQpAI/suAAQsuAApAo/g");
	var mask_graphics_37 = new cjs.Graphics().p("AUjc0QqfqfAAu1QAAuzKfqfQKfqeO1AAQO1AAKfKeQKfKfAAOzQAAO1qfKfQqfKfu1AAQu1AAqfqfg");
	var mask_graphics_38 = new cjs.Graphics().p("AOad4Qr+r/AAw7QAAw6L+r+QL/r+Q7AAQQ8AAL+L+QL/L+AAQ6QAAQ7r/L/Qr+L+w8AAQw7AAr/r+g");
	var mask_graphics_39 = new cjs.Graphics().p("EAISAgfQtctdAAzCQAAzBNcteQNdtdTDAAQTCAANeNdQNdNeAATBQAATCtdNdQteNezCAAQzDAAtdteg");
	var mask_graphics_40 = new cjs.Graphics().p("EACJAkFQu7u8AA1JQAA1IO7u9QO9u8VJAAQVJAAO9O8QO9O9AAVIQAAVJu9O8Qu9O91JAAQ1JAAu9u9g");
	var mask_graphics_41 = new cjs.Graphics().p("EgD+AnrQwcwbAA3QQAA3PQcwcQQawcXQAAQXQAAQcQcQQdQcAAXPQAAXQwdQbQwcQd3QAAQ3QAAwawdg");
	var mask_graphics_42 = new cjs.Graphics().p("EgKGArRQx8x7AA5WQAA5VR8x8QR5x7ZXAAQZXAAR7R7QR7R8AAZVQAAZWx7R7Qx7R85XAAQ5XAAx5x8g");
	var mask_graphics_43 = new cjs.Graphics().p("EgQPAu3QzbzaAA7dQAA7cTbzbQTZzabdAAQbeAATaTaQTbTbAAbcQAAbdzbTaQzaTb7eAAQ7dAAzZzbg");
	var mask_graphics_44 = new cjs.Graphics().p("EgWYAyeQ0606AA9kQAA9jU606QU606djAAQdkAAU6U6QU6U6AAdjQAAdk06U6Q06U69kAAQ9jAA0606g");
	var mask_graphics_45 = new cjs.Graphics().p("EgcgA2EQ2Z2ZAA/rQAA/qWZ2ZQWZ2ZfpAAQfrAAWZWZQWZWZAAfqQAAfr2ZWZQ2ZWZ/rAAQ/pAA2Z2Zg");
	var mask_graphics_46 = new cjs.Graphics().p("EgipA5qUgX4gX5AAAghxUAAAghwAX4gX5UAX4gX4AhwAAAUAhyAAAAX4AX4UAX5AX5AAAAhwUAAAAhxgX5AX5UgX4AX4ghyAAAUghwAAAgX4gX4g");
	var mask_graphics_47 = new cjs.Graphics().p("EgoyA9QUgZXgZYAAAgj4UAAAgj3AZXgZYUAZYgZXAj3AAAUAj4AAAAZYAZXUAZXAZYAAAAj3UAAAAj4gZXAZYUgZYAZXgj4AAAUgj3AAAgZYgZXg");
	var mask_graphics_48 = new cjs.Graphics().p("Egu7BA2Uga2ga3AAAgl/UAAAgl+Aa2ga3UAa4ga3Al9AAAUAl/AAAAa3Aa3UAa3Aa3AAAAl+UAAAAl/ga3Aa3Uga3Aa3gl/AAAUgl9AAAga4ga3g");
	var mask_graphics_49 = new cjs.Graphics().p("Eg1DBEcUgcWgcWAAAgoGUAAAgoFAcWgcWUAcWgcWAoEAAAUAoGAAAAcWAcWUAcXAcWAAAAoFUAAAAoGgcXAcWUgcWAcWgoGAAAUgoEAAAgcWgcWg");
	var mask_graphics_50 = new cjs.Graphics().p("Eg7MBICUgd1gd2AAAgqMUAAAgqLAd1gd2UAd2gd1AqLAAAUAqMAAAAd2Ad1UAd1Ad2AAAAqLUAAAAqMgd1Ad2Ugd2Ad1gqMAAAUgqLAAAgd2gd1g");
	var mask_graphics_51 = new cjs.Graphics().p("EhBVBLoUgfUgfVAAAgsTUAAAgsSAfUgfVUAfVgfVAsSAAAUAsUAAAAfUAfVUAfVAfVAAAAsSUAAAAsTgfVAfVUgfUAfVgsUAAAUgsSAAAgfVgfVg");
	var mask_graphics_52 = new cjs.Graphics().p("EhHdBPOUgg0gg0AAAguaUAAAguZAg0gg0UAg0gg0AuYAAAUAubAAAAg0Ag0UAg0Ag0AAAAuZUAAAAuagg0Ag0Ugg0Ag0gubAAAUguYAAAgg0gg0g");
	var mask_graphics_53 = new cjs.Graphics().p("EhNmBS0UgiTgiTAAAgwhUAAAgwgAiTgiTUAiTgiTAwgAAAUAwhAAAAiTAiTUAiTAiTAAAAwgUAAAAwhgiTAiTUgiTAiTgwhAAAUgwgAAAgiTgiTg");
	var mask_graphics_54 = new cjs.Graphics().p("EhTvBWaUgjygjzAAAgynUAAAgymAjygjzUAjzgjzAymAAAUAyoAAAAjyAjzUAjzAjzAAAAymUAAAAyngjzAjzUgjyAjzgyoAAAUgymAAAgjzgjzg");
	var mask_graphics_55 = new cjs.Graphics().p("EhZ4BaAUglRglSAAAg0uUAAAg0tAlRglSUAlTglSA0sAAAUA0vAAAAlSAlSUAlSAlSAAAA0tUAAAA0uglSAlSUglSAlSg0vAAAUg0sAAAglTglSg");
	var mask_graphics_56 = new cjs.Graphics().p("EhdlBdmUgmxgmxAAAg21UAAAg20AmxgmxUAmxgmxA20AAAUA21AAAAmxAmxUAmxAmxAAAA20UAAAA21gmxAmxUgmxAmxg21AAAUg20AAAgmxgmxg");
	var mask_graphics_57 = new cjs.Graphics().p("EhhLBhMUgoRgoQAAAg48UAAAg47AoRgoQUAoQgoRA47AAAUA48AAAAoQAoRUAoRAoQAAAA47UAAAA48goRAoQUgoQAoRg48AAAUg47AAAgoQgoRg");
	var mask_graphics_58 = new cjs.Graphics().p("EhkyBkyUgpvgpwAAAg7CUAAAg7CApvgpwUApwgpvA7CAAAUA7CAAAApwApvUApwApwAAAA7CUAAAA7CgpwApwUgpwApwg7CAAAUg7CAAAgpwgpwg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(30).to({graphics:mask_graphics_30,x:408.8,y:137.7}).wait(1).to({graphics:mask_graphics_31,x:425,y:154}).wait(1).to({graphics:mask_graphics_32,x:441.3,y:170.2}).wait(1).to({graphics:mask_graphics_33,x:457.6,y:186.5}).wait(1).to({graphics:mask_graphics_34,x:473.8,y:202.8}).wait(1).to({graphics:mask_graphics_35,x:490.1,y:219}).wait(1).to({graphics:mask_graphics_36,x:506.4,y:235.3}).wait(1).to({graphics:mask_graphics_37,x:522.6,y:251.6}).wait(1).to({graphics:mask_graphics_38,x:538.9,y:267.8}).wait(1).to({graphics:mask_graphics_39,x:555.2,y:274.1}).wait(1).to({graphics:mask_graphics_40,x:571.4,y:274.1}).wait(1).to({graphics:mask_graphics_41,x:587.7,y:274.1}).wait(1).to({graphics:mask_graphics_42,x:604,y:274.1}).wait(1).to({graphics:mask_graphics_43,x:620.2,y:274.1}).wait(1).to({graphics:mask_graphics_44,x:636.5,y:274.1}).wait(1).to({graphics:mask_graphics_45,x:652.8,y:274.1}).wait(1).to({graphics:mask_graphics_46,x:669,y:274.1}).wait(1).to({graphics:mask_graphics_47,x:685.3,y:274.1}).wait(1).to({graphics:mask_graphics_48,x:701.6,y:274.1}).wait(1).to({graphics:mask_graphics_49,x:717.8,y:274.1}).wait(1).to({graphics:mask_graphics_50,x:734.1,y:274.1}).wait(1).to({graphics:mask_graphics_51,x:750.4,y:274.1}).wait(1).to({graphics:mask_graphics_52,x:766.6,y:274.1}).wait(1).to({graphics:mask_graphics_53,x:782.9,y:274.1}).wait(1).to({graphics:mask_graphics_54,x:799.2,y:274.1}).wait(1).to({graphics:mask_graphics_55,x:815.4,y:274.1}).wait(1).to({graphics:mask_graphics_56,x:816.2,y:274.1}).wait(1).to({graphics:mask_graphics_57,x:816.2,y:274.1}).wait(1).to({graphics:mask_graphics_58,x:816.2,y:274.1}).wait(34));

	// lines
	this.instance_11 = new lib.Bitmap1();
	this.instance_11.setTransform(191.7,108.9);
	this.instance_11._off = true;

	this.instance_11.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(30).to({_off:false},0).wait(62));

	// Layer 7 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_24 = new cjs.Graphics().p("EA8EAVxQhAhBAAhbQAAhbBAhBQBBhABbAAQBbAABBBAQBABBAABbQAABbhABBQhBBAhbAAQhbAAhBhAg");
	var mask_1_graphics_25 = new cjs.Graphics().p("EA6tAWEQhWhWAAh5QAAh5BWhWQBVhWB6AAQB5AABWBWQBWBWAAB5QAAB5hWBWQhWBWh5AAQh6AAhVhWg");
	var mask_1_graphics_26 = new cjs.Graphics().p("EA5VAWXQhrhrAAiXQAAiXBrhsQBrhrCYAAQCXAABrBrQBrBsAACXQAACXhrBrQhrBriXAAQiYAAhrhrg");
	var mask_1_graphics_27 = new cjs.Graphics().p("EA3+AWqQiBiAAAi1QAAi2CBiAQCAiAC1AAQC2AACACAQCACAAAC2QAAC1iACAQiACBi2AAQi1AAiAiBg");
	var mask_1_graphics_28 = new cjs.Graphics().p("EA2mAW+QiViWAAjTQAAjUCViVQCWiWDTAAQDTAACWCWQCVCVABDUQgBDTiVCWQiWCVjTAAQjTAAiWiVg");
	var mask_1_graphics_29 = new cjs.Graphics().p("EA1PAXRQirirAAjxQAAjyCrirQCqiqDyAAQDxAACrCqQCrCrAADyQAADxirCrQirCrjxAAQjyAAiqirg");
	var mask_1_graphics_30 = new cjs.Graphics().p("EAz3AXkQjAjAAAkPQAAkQDAjAQDAjAEQAAQEPAADADAQDADAAAEQQAAEPjADAQjADAkPAAQkQAAjAjAg");
	var mask_1_graphics_31 = new cjs.Graphics().p("EAygAX3QjWjVAAkuQAAktDWjVQDVjWEtAAQEuAADVDWQDVDVAAEtQAAEujVDVQjVDVkuAAQktAAjVjVg");
	var mask_1_graphics_32 = new cjs.Graphics().p("EAxIAYKQjqjqAAlMQAAlLDqjrQDrjqFLAAQFLAADrDqQDqDrAAFLQAAFMjqDqQjrDrlLAAQlLAAjrjrg");
	var mask_1_graphics_33 = new cjs.Graphics().p("EAvxAYdQkAj/AAlqQAAlpEAkAQD/kAFqAAQFpAAEAEAQEAEAAAFpQAAFqkAD/QkAEAlpAAQlqAAj/kAg");
	var mask_1_graphics_34 = new cjs.Graphics().p("EAuZAYxQkVkVAAmIQAAmHEVkVQEVkTGIAAQGHAAEVETQEVEVAAGHQAAGIkVEVQkVEVmHAAQmIAAkVkVg");
	var mask_1_graphics_35 = new cjs.Graphics().p("EAtCAZEQkrkqAAmmQAAmlErkrQEqkoGlAAQGmAAEqEoQEqErAAGlQAAGmkqEqQkqEqmmAAQmlAAkqkqg");
	var mask_1_graphics_36 = new cjs.Graphics().p("EArqAZXQk/k/gBnEQABnDE/lAQFAk9HDgBQHDABFAE9QE/FAABHDQgBHEk/E/QlAE/nDABQnDgBlAk/g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(24).to({graphics:mask_1_graphics_24,x:422.1,y:145.8}).wait(1).to({graphics:mask_1_graphics_25,x:425.7,y:149.8}).wait(1).to({graphics:mask_1_graphics_26,x:429.3,y:153.9}).wait(1).to({graphics:mask_1_graphics_27,x:433,y:157.9}).wait(1).to({graphics:mask_1_graphics_28,x:436.6,y:162}).wait(1).to({graphics:mask_1_graphics_29,x:440.2,y:166}).wait(1).to({graphics:mask_1_graphics_30,x:443.8,y:170}).wait(1).to({graphics:mask_1_graphics_31,x:447.5,y:174.1}).wait(1).to({graphics:mask_1_graphics_32,x:451.1,y:178.1}).wait(1).to({graphics:mask_1_graphics_33,x:454.7,y:182.2}).wait(1).to({graphics:mask_1_graphics_34,x:458.3,y:186.2}).wait(1).to({graphics:mask_1_graphics_35,x:462,y:190.3}).wait(1).to({graphics:mask_1_graphics_36,x:465.6,y:194.3}).wait(56));

	// swiss
	this.instance_12 = new lib.Tween3("synched",0);
	this.instance_12.setTransform(822.2,279.6);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.instance_12.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(24).to({_off:false},0).to({alpha:1},12,cjs.Ease.get(-0.53)).wait(56));

	// base map
	this.instance_13 = new lib.Tween11("synched",0);
	this.instance_13.setTransform(831,492.5);
	this.instance_13.alpha = 0;

	this.instance_14 = new lib.Tween12("synched",0);
	this.instance_14.setTransform(831,412.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_13}]}).to({state:[{t:this.instance_13}]},18).to({state:[{t:this.instance_14}]},73).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({y:412.5,alpha:1},18,cjs.Ease.get(1)).to({_off:true},73).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(691,244.5,1920,1073);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;