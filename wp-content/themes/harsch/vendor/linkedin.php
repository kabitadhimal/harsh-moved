<?php

use OAuth\OAuth2\Service\Linkedin;
use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;


//require_once __DIR__ . '/bootstrap.php';
require get_template_directory() . '\vendor\lusitanian\oauth\src\OAuth\bootstrap.php';
// Session storage
$storage = new Session();

// Setup the credentials for the requests
$credentials = new Credentials(
    $servicesCredentials['linkedin']['key'],
    $servicesCredentials['linkedin']['secret'],
    $currentUri->getAbsoluteUri()
);

$linkedinService = $serviceFactory->createService('linkedin', $credentials, $storage, array('r_basicprofile'));

if (!empty($_GET['code'])) {
    // retrieve the CSRF state parameter
    $state = isset($_GET['state']) ? $_GET['state'] : null;

    // This was a callback request from linkedin, get the token
    $token = $linkedinService->requestAccessToken($_GET['code'], $state);

    // Send a request with it. Please note that XML is the default format.
    $result = json_decode($linkedinService->request('/people/~?format=json'), true);

    // Show some of the resultant data
    echo 'Your linkedin first name is ' . $result['firstName'] . ' and your last name is ' . $result['lastName'];

} elseif (!empty($_GET['go']) && $_GET['go'] === 'go') {
    $url = $linkedinService->getAuthorizationUri();
    header('Location: ' . $url);
} else {
    $url = $currentUri->getRelativeUri() . '?go=go';
    echo "<a href='$url'>Login with Linkedin!</a>";
}



//require get_template_directory() .'\vendor\happyr\linkedin\src\LinkedIn.php';
//require_once "vendor/autoload.php";
/*require_once get_template_directory() .'\vendor\autoload.php';
$linkedIn=new Happyr\LinkedIn\LinkedIn('81fxffkxjmq45m', 'DcLlUhOpqVxgiTw6');

if ($linkedIn->isAuthenticated()) {
    //we know that the user is authenticated now. Start query the API
    $user=$linkedIn->get('v1/people/~:(firstName,lastName)');
    echo "Welcome ".$user['firstName'];

    exit();
} elseif ($linkedIn->hasError()) {
    echo "User canceled the login.";
    exit();
}

//if not authenticated
$url = $linkedIn->getLoginUrl();
echo "<a href='$url'>Login with LinkedIn</a>";*/