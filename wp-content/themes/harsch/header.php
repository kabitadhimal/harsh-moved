<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<html <?php language_attributes(); ?>>
<head>
        <!-- <meta charset="utf-8"> -->
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <!--<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>-->
       
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
       
      
<?php wp_head(); ?>



<!-- Google Tag Manager -->


<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-579Q58M');</script>

<!-- End Google Tag Manager -->




<script type="text/javascript">
     var $ = jQuery.noConflict();
</script>
</head>

<body <?php body_class(); ?> >
    
    <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-579Q58M" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


 <!-- NAV START -->
    <nav class="navbar" role="navigation">
      <div class="container text-center main-menu">
        <div class="logo">
        	<?php $logo=get_field('logo','option'); ?>
            <figure><a href="<?php echo icl_get_home_url();?>"><img src="<?php echo $logo['url'];?>" alt="<?php echo $logo['alt'];?>"></a></figure>
        </div>
        <?php
        global $post;
        $cur_id = $post->ID;
        
        $menu1 = wp_get_nav_menu_items('Menu 1',array(
           'posts_per_page' => -1,
           'meta_key' => '_menu_item_object_id',
           'meta_value' => $cur_id,
        ));

        $menu_id=1;
        //echo $menu_id;
         
           $menus = wp_get_menu_array('Menu 1');
        
              // echo '<pre>'; print_r($menus); echo '</pre>';
         ?>
           <div class="mainMenu">
                 <?php if(!empty($menus)):
                        ?>
                           
                        <ul class="mainMenu--holder">
                               <?php
                                foreach($menus as $menu):
                                  $id2=$menu['ID'];
                                if(!empty($menu['classes']) && in_array('mainMenu',$menu['classes'])):
                                  ?>     
                                      <li class="mainMenu--child <?php if( $id2== $menu_id) echo 'active';?>"><a href="<?php echo $menu['url']?>"><?php echo $menu['title']?></a>
                                          <ul class="submenu">
                                            <?php if(!empty($menu['children'])):
                                                      foreach($menu['children'] as $m):
                                                        $id=$m['ID'];
                                                      //echo $id;
                                                        ?>
                                                       <li class="submenu--item <?php if( $id== $menu_id) echo 'active';?>"><a href="<?php echo $m['url']?>"><?php echo $m['title']?></a></li>
                                             
                                            <?php endforeach;
                                            endif; ?>
                                          </ul>
                                        <!-- <div class="mega--menu animated fadeIn">
                                          <div class="container container__small">

                                            <?php
                                                  foreach($menu['children'] as $m):
                                                      $id4=$m['ID'];
                                                        ?>
                                                        <div class="col col--2 <?php if( $id4== $menu_id) echo 'active';?>">
                                                          <a href="<?php echo $m['url']?>" class="<?php echo $m['classes'][0];?>">
                                                          <figure>
                                                            <img src="<?php echo $m['imageurl'];?>">
                                                          </figure>
                                                          <div class="small--title"><?php echo $m['title']?></div>
                                                          <p>
                                                            <?php echo $m['description']?>
                                                          </p>
                                                          </a>
                                                        </div>
                                            <?php endforeach; ?>
                                            
                                          </div>
                                        </div> -->

                                      </li>
                                             
                            <?php endif;
                                 endforeach;?>  
                          </ul>
                   
                 </div>

      <div class="secondaryMenu">
        <?php   

        foreach($menus as $menu):
          $id1=$menu['ID'];
         if(!empty($menu['classes']) && in_array('secondaryMenu',$menu['classes'])):?>
                   

                        <div class="secondaryMenu--child <?php if( $id1== $menu_id) echo 'active';?>"><a href="<?php echo $menu['url']?>" class="secondaryMenu--item"><?php echo $menu['title']?></a></div>
                       
                  
      <?php endif;
      endforeach;?>
        </div>

        <div class="loginMenu">
          <div class="loginMenu--child">
            <select class="customSelect"  onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">

             <?php
                      $lang = icl_get_languages();

                      if(!empty($lang)):

                        foreach ($lang as $multi_lang) {

                          if($multi_lang['active'] == 1):
                            echo "<option value='".$multi_lang['url']."' selected='selected'>".strtoupper($multi_lang['code'])."</option>";
                          else:
                            echo "<option value='".$multi_lang['url']."'>".strtoupper($multi_lang['code'])."</option>";
                          endif;

                        }
                       endif;
                       ?>

             <!--  <option>EN</option>
              <option>FR</option> -->
            </select>
          </div>
          <?php 
            $header_left_text = get_field('left_text','option');
            $header_left_text_link = get_field('left_text_link','option');

           ?>
          <div class="loginMenu--child"><a href="<?php echo $header_left_text_link; ?>" class="login-btn"><?php echo $header_left_text; ?></a></div>
        </div>

     <?php endif?>
      </div>
        <?php if(!empty($menus)):
               ?>
                          <div class="stickyMenu">
                            <div class="stickyMenu--holder text-center">
                              <div class="stickyMenu--logo">
                                  <figure><a href="<?php echo home_url();?>"><img src="<?php echo get_template_directory_uri();?>/img/logo-small.png"></a></figure>
                              </div>
                              <div class="responsiveMenu visible-xsmall">
                                <div class="hamburger-icon"></div>
                              </div>
                              <div class="stickyMenu--links">
                                  <ul class="stickyMenu--items">
                                    <?php foreach($menus as $menu):
                                    $id3=$menu['ID'];
                                    if(!empty($menu['children'])){
                                      $clas="with--child";
                                    }else{
                                      $clas="";
                                    }
                                    ?>
                                      <li class="stickyMenu--items--item <?php echo $clas; if( $id3== $menu_id) echo ' active';?>"><a href="<?php echo $menu['url']?>"><?php echo $menu['title']?></a>
                                        <?php if(!empty($menu['children'])):?>
                                        <div class="sticky--submenu animated fadeIn">
                                        <div class="container container__small">
                                        <?php
                                                      foreach($menu['children'] as $m):

                                                        //print_r($m);
                                                        $id4=$m['ID'];
                                                        ?>
                                                        <div class="col col--2 <?php if( $id4== $menu_id) echo 'active';?>">
                                                          <a href="<?php echo $m['url']?>" class="<?php echo $m['classes'][0];?>">
                                                          <figure>
                                                            <img src="<?php echo $m['imageurl'];?>">
                                                          </figure>
                                                          <div class="small--title"><?php echo $m['title']?></div>
                                                          <p>
                                                            <?php echo $m['description'];?>
                                                          </p>
                                                          </a>
                                                        </div>
                                                  <?php endforeach; ?>
                                         </div>                                                  
                                        </div>
                                        <?php  endif;?>  
                                      </li>
                                    <?php  endforeach; ?>
                                      
                                     
                                  <!--
                                  <li class="stickyMenu--items--item with--child lang">
                                  <?php
                                    $lang = icl_get_languages();
                                    $inactive_menu = "";
                                    if(!empty($lang)):
                                      foreach ($lang as $multi_lang) 
                                      {
                                        if($multi_lang['active'] == 1):
                                          echo "<a href='".$multi_lang['url']."'>".strtoupper($multi_lang['code'])."</a>";
                                        else:
                                          $inactive_menu.=  "<li><a href='".$multi_lang['url']."'>".strtoupper($multi_lang['code'])."</a></li>";
                                        endif;
                                      }
                                     endif;
                                   ?>
                                    <div class="sticky--submenu animated fadeIn">
                                      <ul>
                                        <?php echo $inactive_menu; ?> 
                                      </ul>                                    
                                    </div>
                                  </li>
                                  -->
                                 
								 <?php if(ICL_LANGUAGE_CODE=='fr') { ?>
								 <li class="sticky--l-contact contact-btn mobile-only"><a><img src="<?php echo get_template_directory_uri();?>/images/icon-contact.png"></a></li>
								 <li class="sticky--l-contact desktop-only"><a href="https://www.harsch.ch/contact/#bureaux"><img src="<?php echo get_template_directory_uri();?>/images/icon-contact.png"></a></li>
								<li class="sticky--l-quote"><a  href="<?php echo home_url(); ?>/devis-gratuit-pour-un-demenagement/"><img src="<?php echo get_template_directory_uri();?>/images/icon-quote.png"></a> </li>
								 <?php } else if(ICL_LANGUAGE_CODE=='de') { ?>
								 <li class="sticky--l-contact contact-btn mobile-only"><a><img src="<?php echo get_template_directory_uri();?>/images/icon-contact.png"></a></li>
								 <li class="sticky--l-contact desktop-only"><a href="https://www.harsch.ch/de/kontakt/#genf"><img src="<?php echo get_template_directory_uri();?>/images/icon-contact.png"></a></li>
								 <!-- quote page DE -->
								 <?php } else { ?>
								<li class="sticky--l-contact contact-btn mobile-only"><a><img src="<?php echo get_template_directory_uri();?>/images/icon-contact.png"></a> </li>
								<li class="sticky--l-contact desktop-only"><a href="https://www.harsch.ch/en/contact/#offices"><img src="<?php echo get_template_directory_uri();?>/images/icon-contact.png"></a></li>
								<li class="sticky--l-quote"><a  href="<?php echo home_url(); ?>/free-moving-quote/"><img src="<?php echo get_template_directory_uri();?>/images/icon-quote.png"></a> </li>
								 <?php } ?>

                              </ul>
                              </div>
                            </div>
                          </div>
    <?php
    endif;?>
    </nav>
    <!-- NAV END -->

    <div id="block--quote" class="zoom-anim-dialog mfp-hide block block--quote">
        <h2 class="block--quote__title"><?php _e("Contactez-nous","harsch"); ?></h2>
        
        <?php if( have_rows('contact_phone_numbers', 'option') ): ?>


            <?php while( have_rows('contact_phone_numbers', 'option') ): the_row(); ?>
        
               <div class="block--quote_c">
                    <h2 class="block--quote__titlec"><?php the_sub_field('title'); ?></h2>
                    <p><a href="tel:<?php the_sub_field('contact_number'); ?>"><?php the_sub_field('contact_number'); ?></a></p>
                </div>
        
            <?php endwhile; ?>
        

        <?php endif; ?>

    </div>


	
		
		
