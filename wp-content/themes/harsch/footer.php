<!-- FOOTER START -->
    <?php if ( is_active_sidebar( 'pre-footer' ) ) { ?>
		<div class="pre-footer">
			<div class="container container__medium">
				<div class="row">
					<?php dynamic_sidebar( 'pre-footer' ); ?>
				</div>
			</div>
		</div>
	<?php } ?>

	<footer>
        <div class="container container__medium">
            
            
            <div class="footerTop">
                <div class="footerTop--col footerLogo">
                <?php $logo=get_field('logo','option'); ?>
                <img src="<?php echo $logo['url'];?>" alt="<?php echo $logo['alt'];?>">
                </div>
                <div class="footerTop--col footerMenu footerMenu--1 hidden-xsmall">
                    <a class="footerMenu--link footerMenu--link__title"><?php _e('Services','harsch'); ?></a>
                    <?php echo get_field('internal_links_column','option');?>
                </div>
                <div class="footerTop--col footerMenu footerMenu--2 hidden-xsmall">
                    <a class="footerMenu--link footerMenu--link__title"><?php _e('Nos adresses','harsch'); ?></a>
                    <?php echo get_field('external_links_column','option');?>
                </div>
                <div class="footerTop--col footerExtra">
                    <div class="footerExtra--social">
                        <a href="<?php echo get_field('rss_link','option');?>"><i class="fa fa-rss"></i></a>
                        <a href="<?php echo get_field('linkedin_link','option');?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a href="https://www.facebook.com/Harsch-The-Art-of-Moving-Forward-1545925898758875/" target="_blank"><i class="fa fa-facebook"></i></a>

                    </div>
                    <?php
                        $languages = icl_get_languages('skip_missing=0&orderby=code');
                        //unset($languages['de']);

                        ?>
                        <div class="footerExtra--language">
                        <select class="customSelect" id="customSelect">
                        <?php
                        if(!empty($languages)){
                          foreach($languages as $l){            
                                  if($l['active']):
                                    ?>
                                    <option value="<?php echo $l['url']; ?>">
                                    <?php //echo icl_disp_language($l['native_name'], $l['translated_name']); ?>
                                    <?php echo icl_disp_language($l['native_name']); ?>
                                    </option>
                            <?php 
                             
                             endif; 

                                }
                          foreach($languages as $l){            
                                  if(!$l['active']):
                                    ?>
                                    <option value="<?php echo $l['url']; ?>">
                                    <?php //echo icl_disp_language($l['native_name'], $l['translated_name']); ?>
                                    <?php echo icl_disp_language($l['native_name']); ?>
                                    </option>
                            <?php endif;      
                                }
                            }
                        ?>

                        </select>
                        </div>
                    
                   
                    <div class="footerExtra--credit">
                        <a href="https://www.procab.ch/" target="_blank">Website by Procab</a>
                    </div>
                </div>
            </div>
            <div class="footerIcons text-center hidden-xsmall">
            <?php if( have_rows('partners_logos','option') ):
                    while ( have_rows('partners_logos','option') ) : the_row();
                   
                    $image = get_sub_field('colored_picture','option');
                    $imglink= get_sub_field('picture_link');
                    echo '<a href="'.$imglink.'" target="_blank" class="footerIcons--icon"><figure><img src="' . $image['url'] . '" alt="' . $image['alt'] . '"></figure></a>';
                    endwhile;
                  endif;
            ?>
               
            </div>
        </div>
    </footer>
   <?php
  $about_page_id = array('2628','306');
  $check_id = $post->ID;
  $foter_about_js = '';
  if(in_array($check_id,$about_page_id))
      $foter_about_js='yes';
  
  ?>
    <!-- FOOTER END -->

        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri();?>/js/vendor/jquery-1.11.2.js"><\/script>')</script>-->

        <script src="<?php echo get_template_directory_uri();?>/js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo get_template_directory_uri();?>/js/vendor/jquery.waypoints.js"></script>
        <script src="<?php echo get_template_directory_uri();?>/js/vendor/slick.min.js"></script>

        <script src="<?php echo get_template_directory_uri();?>/js/vendor/swiper.js"></script>
        <script src="<?php echo get_template_directory_uri();?>/js/vendor/fancybox/jquery.fancybox.min.js"></script>
        <script src="<?php echo get_template_directory_uri();?>/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. 
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>-->

        <script type="text/javascript">
            $(window).load(function(){
				/*$("#customSelect").change(function(){
				window.location.href = $(this).attr('rel');
				});
*/
                var $selectOptions = $('.footerExtra--language .select-options, .select__category'),
                    $selectOptionsChildren = $selectOptions.find('li');
                $selectOptionsChildren.each(function(index, element) {
                    var $self = $(this),
                        selfRel = $self.attr('rel'),
                        selfText = $self.text();

                    $self.html('<a href="'+ selfRel +'">'+ selfText +'</a>')
                });
			})
        </script>

        <?php
        if(@$foter_about_js !=''){
            ?>
            <!-- map -->

            <script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
            <script src="<?php echo get_template_directory_uri();?>/js/map.js"></script>
            <script>
            var autoStartMap = false;
            var canvas, stage, exportRoot;
            function init() {
              // --- write your JS code here ---
              
              canvas = document.getElementById("canvas");
              images = images||{};
              ss = ss||{};

              var loader = new createjs.LoadQueue(false);
              loader.addEventListener("fileload", handleFileLoad);
              loader.addEventListener("complete", handleComplete);
              loader.loadFile({src:"images/map_atlas_.json", type:"spritesheet", id:"map_atlas_"}, true);
              loader.loadManifest(lib.properties.manifest);
            }

            function handleFileLoad(evt) {
              if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
            }

            function handleComplete(evt) {
              var queue = evt.target;
              ss["map_atlas_"] = queue.getResult("map_atlas_");
              exportRoot = new lib.mapfin2();

              stage = new createjs.Stage(canvas);
              stage.addChild(exportRoot);
              stage.update();

              createjs.Ticker.setFPS(lib.properties.fps);
              createjs.Ticker.addEventListener("tick", stage);

              if(autoStartMap) window.playAnimation();
            }


            function resumeMapAnimation(){
              if(typeof(window.playAnimation) == 'undefined'){
                autoStartMap = true;
              }else{
                window.playAnimation();
              }
            }


            </script>

            <script>init();</script>
            <style>
            .map-canvas-wrap{
              max-width: 1200px;
              margin: 0 auto;
            }
            .map-canvas{
              width: 100%; height: auto;
              background: #f4f4f4;
            }
            </style>
            <!--/map-->

        <?php
        }
        ?>
<?php wp_footer(); ?>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="contactpopup" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<section>
				<p><strong>Genève</strong><br><a href="tel:+41223004300">+41 22 300 43 00</a></p>
				<p><strong>Vaud – Gland</strong><br><a href="tel:+41223642070">+41 22 364 20 70</a></p>
				<p><strong>Vaud – Lausanne</strong><br><a href="tel:+41213121145">+41 21 312 11 45</a></p>
				<p><strong>Zurich/Basel</strong><br><a href="tel:+41435343128">+41 43 534 31 28</a></p>
				<a class="modal-close popup-close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle"></i></a>
			</section>
		</div>
	</div>
</div>

<div class="side-menu">
	<?php wp_nav_menu( array('theme_location' => 'side') ); ?>
</div>

    </body>
</html>



	
