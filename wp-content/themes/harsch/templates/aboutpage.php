<?php
/**
 * Template Name: About page template 
 *
 * @package WordPress
 */

get_header(); 
//global $post;
?>

<?php
if( have_rows('about') ):
    while ( have_rows('about') ) : the_row();
		
		if( get_row_layout() == 'banner_section' ):
			$banner_text=get_sub_field('banner_text');
			$banner_image=get_sub_field('banner_image');
			?>
			<!-- BANNER START -->
		<section class="banner banner__cms">
		    <div class="banner--image">
		        <figure>
		        <?php if($banner_image!=''){?>	
		        <img src="<?php echo $banner_image['url'];?>">
		        <?php } else{ ?>
		        <img src="<?php echo get_template_directory_uri();?>/img/cmsbg.png">
		        <?php } ?>
		        </figure>
		    </div>
		    <div class="banner--content">
		        <div class="container container__medium">
		            <div class="main--title main--title__white">
		            <?php echo $banner_text;
		            	?>
		            </div>
		        </div>
		    </div>
		    <div class="banner--overlay">
		        <figure><img src="<?php echo get_template_directory_uri()?>/img/banner-bottom.png"></figure>
		    </div>
		    <div class="scroller">
		        <figure><img src="<?php echo get_template_directory_uri()?>/img/down.png"></figure>
		    </div>
		</section>
			<!-- BANNER END -->
		<?php		
		endif;	



		if( get_row_layout() == 'intro_section' ):
			$title=get_sub_field('title');
			$short_content=get_sub_field('short_content');
			$background_image=get_sub_field('background_image');
			?>
			<!-- INTRO START -->
			<section class="intro" style="background-image: url(<?php echo $background_image['url']; ?>);">
			    <div class="container container__small">
			        <h2 class="text-center"><?php echo $title; ?></h2>
			        <div class="col--single text-center">
			            <p><?php echo $short_content; ?></p>
			        </div>
			    </div>    
			</section>
			<!-- INTRO END -->
		<?php	
		endif;


		if( get_row_layout() == 'spaces' ):

			$size = "";
			$size = get_sub_field('size');
			$color = get_sub_field('color');

			$color_style = "";
			if(!empty($color))
				$color_style = "theme__".$color;
			
			?>

			<div class="spacing__<?php echo $size; ?> <?php echo $color_style; ?>"></div>

		<?php
		endif;
		

		if( get_row_layout() == 'timeline_section' ):
			$timeline_section_title=get_sub_field('timeline_section_title');
			?>
			<!-- HISTORY SLIDER START -->
			<section class="slider history--slider theme__lightGrey">
			    <div class="spacing__medium theme__lightGrey"></div>
			    <div class="container container__medium">
			        <div class="small--title small--title--topborder"><?php echo $timeline_section_title;?></div>
			        <div class="spacing__small"></div>
			        <div class="slide--history">
			<?php
			$background_picture=get_sub_field('background_picture');
			if( have_rows('timeline_slider') ):
			    while ( have_rows('timeline_slider') ) : the_row();
			    	$timeline_title=get_sub_field('timeline_title');
					$background_picture = get_sub_field('background_picture');
					$year_date=get_sub_field('year_date');
					$right_text_content=get_sub_field('right_text_content');
					$left_picture=get_sub_field('left_picture');
					?>
					<div data-text="<?php echo $year_date; ?>" class="slide--history--background" style="background-image: url(<?php echo $background_picture['url']; ?>);">
		              

                            <?php if(!empty($left_picture['url'])) : ?>
		              <div class="col col--2">
		                  <figure><img src="<?php echo $left_picture['url']; ?>"></figure>
		              </div>
		            <?php endif; ?>


		              <div class="col col--2 <?php echo (empty($left_picture['url']))?'block--center':''; ?>">
		                <div class="slide--history--container">
		                    <svg width="160" height="70">
		                      <text x="0" y="60" text-anchor="start" fill="none" stroke-width="2" stroke="black"><?php echo $year_date; ?></text>
		                  </svg>
		                  <div class="small--title"><?php echo $timeline_title; ?></div>
		                  <p><?php echo $right_text_content; ?></p>
		                </div>
		              </div>
		            </div>
				<?php	
				endwhile;
				endif;
				?>
				</div>
			    </div>
			    <div class="spacing__medium theme__lightGrey"></div>

			</section>
			<!-- HISTORY SLIDER END -->
			
			<?php	
		endif;


		if( get_row_layout() == '23_image_and_13_text' ):
			$image_23=get_sub_field('image_23');
			$title_13=get_sub_field('title_13');
			$content_13=get_sub_field('content_13');
			
			?>
			
			<!-- TEAM START -->
			<div class="spacing__large"></div>


			
			<section class="team">
			    <div class="container">
			        <div class="col col--2">
			            <figure><img src="<?php echo $image_23['url']; ?>"></figure>
			        </div>
			        <div class="col col--2">
		            <div class="team--holder">
		                <div class="main--title"><?php echo $title_13; ?></div>
		                <p><?php echo $content_13; ?></p>
		            </div>
		            <ul class="team--members">
			<?php
			if( have_rows('link_table') ):
			    while ( have_rows('link_table') ) : the_row();
					$name = get_sub_field('name');
					$function=get_sub_field('function');
					$linkedin_url_link=get_sub_field('linkedin_url_link');
					?>
					<li class="team--members--item">
                    <div class="small--title"><?php echo $name; ?></div>
                    <p><?php echo $function; ?></p>
                    <a href="<?php echo $linkedin_url_link; ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                	</li>
				<?php	
				endwhile;
			endif;
			?>
					</ul>
	       		    </div>
				 </div>
			</section>
			<!-- TEAM END -->
			<div class="spacing__large"></div>
		<?php
		endif;

		 if( get_row_layout() == 'world_map_section' ):
		  ?>
				<!-- MAP SECTION START -->
				<?php echo get_sub_field('map');?>
				<!-- MAP SECTION END -->	
		<?php 
		endif;


		if( get_row_layout() == '4_block_numbers' ):
			if( have_rows('4_block_number') ):
				?>
				<section class="fourBlocks text-center">
			<?php
			    while ( have_rows('4_block_number') ) : the_row();
					$block_background_image=get_sub_field('block_background_image');
					$text_number=get_sub_field('text_number');
					$link_text=get_sub_field('link_text');
					$link=get_sub_field('link');
					?>
				<!-- FOUR BLOCK START -->
			    <div class="col" style="background-image: url(<?php echo $block_background_image['url'];?>); ">
			    <?php if($link!=''){ ?>
			        <a href="<?php echo $link;?>" class="overlay--border animated fadeIn"></a>
			        <?php } ?>
			        <div class="fourBlocks--holder animated animating animating--up">
			            <div class="">
			            	<svg width="220" height="70">
			                  <text x="110" y="60" text-anchor="middle" fill="none" stroke-width="1" stroke="white"><?php echo $text_number;?></text>
			                </svg>
			               
			            </div>
			            <div class="small--title small--title--topborder  small--title--white"><?php echo $link_text;?></div>
			        </div>
			    </div>
    
    		<?php	
				endwhile;
				?>
			</section>
			<!-- FOUR BLOCK END -->
			<?php	
			endif;
		endif;


		if( get_row_layout() == '3_footers_icons' ){

			//die('test');
				$first_icon_text=get_sub_field('first_icon_text');
				$first_icon_url=get_sub_field('first_icon_url');
				$second_icon_text=get_sub_field('second_icon_text');
				$second_icon_url=get_sub_field('second_icon_url');
				$third_icon_text=get_sub_field('third_icon_text');
				$third_icon_url=get_sub_field('third_icon_url');
		 ?>

		<section>
		 <div class="spacing__medium"></div>
		 <div class="bigIcons container container__small">
            <?php //require get_template_directory() . '/template-parts/footer-icon.php';?>
                <a href="<?php echo $first_icon_url;?>" target="_blank" class="bigIcons--list text-center">
                    <span class="icon icon-bulb"></span>
                    <span class="btn btn--main"><?php echo $first_icon_text;?></span>
                </a>
                <a href="<?php echo $second_icon_url;?>" target="_blank" class="bigIcons--list text-center">
                    <span class="icon icon-letter"></span>
                    <span class="btn btn--main"><?php echo $second_icon_text;?></span>
                </a>
                <a href="<?php echo $third_icon_url;?>" target="_blank" class="bigIcons--list text-center">
                    <span class="icon icon-book"></span>
                    <span class="btn btn--main"><?php echo $third_icon_text;?></span>
                </a>
         </div>
		</section>
		
		<?php }  

		
	endwhile;
endif;


?>
<?php 
//require get_template_directory().'/template-parts/footer-icon.php';
?>

<?php
$about_page_id = array('2628','306');
$check_id = $post->ID;
if(in_array($check_id,$about_page_id)):
		//echo $about_page_id;
		$foter_about_js='yes';
	?>

	 <!-- map -->

            <script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
            <script src="<?php echo get_template_directory_uri();?>/js/map.js"></script>
            <script>
            var autoStartMap = false;
            var canvas, stage, exportRoot;
            function init() {
              // --- write your JS code here ---
              
              canvas = document.getElementById("canvas");
              images = images||{};
              ss = ss||{};

              var loader = new createjs.LoadQueue(false);
              loader.addEventListener("fileload", handleFileLoad);
              loader.addEventListener("complete", handleComplete);
              loader.loadFile({src:"<?php echo get_template_directory_uri();?>/images/map_atlas_.json", type:"spritesheet", id:"map_atlas_"}, true);
              loader.loadManifest(lib.properties.manifest);
            }

            function handleFileLoad(evt) {
              if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
            }

            function handleComplete(evt) {
              var queue = evt.target;
              ss["map_atlas_"] = queue.getResult("map_atlas_");
              exportRoot = new lib.mapfin2();

              stage = new createjs.Stage(canvas);
              stage.addChild(exportRoot);
              stage.update();

              createjs.Ticker.setFPS(lib.properties.fps);
              createjs.Ticker.addEventListener("tick", stage);

              if(autoStartMap) window.playAnimation();
            }


            function resumeMapAnimation(){
              if(typeof(window.playAnimation) == 'undefined'){
                autoStartMap = true;
              }else{
                window.playAnimation();
              }
            }


            </script>

            <script>init();</script>
            <style>
            .map-canvas-wrap{
              max-width: 1200px;
              margin: 0 auto;
            }
            .map-canvas{
              width: 100%; height: auto;
              background: #f4f4f4;
            }
            </style>
            <!--/map-->
	<?php
else:
		$foter_about_js='';
endif;	
get_footer();
?>