<?php
/**
 * Template Name: Home page template 
 *
 * @package WordPress
 */

get_header(); 

?>

<?php
if( have_rows('home') ):
    while ( have_rows('home') ) : the_row();
		if( get_row_layout() == 'slidervideo' ):
			?>
		<!-- BANNER START -->
		<section class="banner banner__home">
			<div class="quick-call">
				<ul>
					<?php if(ICL_LANGUAGE_CODE=='fr') { ?>
						<li class="l-quote"><a  href="https://www.harsch.ch/devis-gratuit-pour-un-demenagement/"><?php _e('Un devis ?','harsch'); ?></a></li>
						<li class="l-contact contact-btn mobile-only"><a><?php _e('Contactez-nous','harsch'); ?></a></li>
						<li class="l-contact desktop-only"><a href="https://www.harsch.ch/contact/#bureaux"><?php _e('Contactez-nous','harsch'); ?></a></li>
					<?php } elseif(ICL_LANGUAGE_CODE=='de') { ?>
						<!--<li class="l-quote"><a  href="<?php echo home_url(); ?>/contact/#form"><?php _e('Un devis ?','harsch'); ?></a></li>-->
						<li class="l-contact contact-btn mobile-only"><a><?php _e('Contactez-nous','harsch'); ?></a></li>
						<li class="l-contact desktop-only"><a href="https://www.harsch.ch/de/kontakt/#genf"><?php _e('Contactez-nous','harsch'); ?></a></li>
					<?php } else { ?>
						<li class="l-quote"><a  href="https://www.harsch.ch/en/free-moving-quote/"><?php _e('Un devis ?','harsch'); ?></a></li>
						<li class="l-contact contact-btn mobile-only"><a><?php _e('Contactez-nous','harsch'); ?></a></li>
						<li class="l-contact desktop-only"><a href="https://www.harsch.ch/en/contact/#offices"><?php _e('Contactez-nous','harsch'); ?></a></li>
					<?php } ?>
				</ul>
			</div>
		<?php
			if (get_sub_field('slider_or_video') == 'slider') {
				if( have_rows('slider_images') ):
			 	echo '<div class="home--slider">';	 	
			    while ( have_rows('slider_images') ) : the_row();
					$slider_image = get_sub_field('slider_image');
					$image_title= get_sub_field('image_title');
					$link= get_sub_field('link');
					?>
					<div class="home--slider--items">
			            <figure><a href="<?php echo $link; ?>"><img src="<?php echo $slider_image['url']; ?>"></a></figure>
			            <div class="banner--content">
			                <div class="container container__medium">
			                    <div class="main--title main--title__white">
			                       <?php echo $image_title; ?>
			                    </div>
			                </div>
			            </div>
			        </div>
				<?php	
				endwhile;
				echo '</div>';
				endif;
			} 
			else{
				$video_url=get_sub_field('video_url');
				$video_cover_image=get_sub_field('video_cover_image');
				?>
				<div class="home--video video--container">
			        <video autoplay loop muted style="background-image: url(<?php echo $video_cover_image['url']; ?>);" poster="<?php echo get_template_directory_uri(); ?>/img/transparent.png">
			            <source src="<?php echo $video_url;?>" type="video/mp4">
			        </video>
			    </div>
			    <div class="banner--content">
		            <div class="container container__medium">
		                <div class="main--title main--title__white">
		                    <?php echo get_sub_field('banner_content');?>
		                </div>
		            </div>
		        </div>
			<?php	
			}			
			?>
			<div class="banner--overlay">
			        <figure><img src="<?php echo get_template_directory_uri(); ?>/img/banner-bottom.png"></figure>
			    </div>
			    <div class="scroller">
			        <figure><img src="<?php echo get_template_directory_uri(); ?>/img/down.png"></figure>
			    </div>
			</section>
			<!-- BANNER END -->
			<div class="spacing__small"></div>
		<?php	
		endif;	


		if( get_row_layout() == 'intro_section' ):
			$title=get_sub_field('title');
			$short_content=get_sub_field('short_content');
			$background_image=get_sub_field('background_image');
			?>
			<!-- INTRO START -->
			<section class="intro" style="background-image: url(<?php echo $background_image['url']; ?>);">
			    <div class="container container__small">
			        <h2 class="text-center"><?php echo $title; ?></h2>
			        <div class="col--single text-center">
			            <p><?php echo $short_content; ?></p>
			        </div>
			    </div>    
			</section>
			<!-- INTRO END -->
		<?php	
		endif;


		if( get_row_layout() == 'solution_section' ):
			$solution_title=get_sub_field('solution_title');
			?>
		<div class="spacing__medium"></div>
		<!-- SOLUTION BLOCK START -->
		<section class="solutions">
		    <div class="container">
		    <div class="small--title small--title--topborder small--title__smallspace text-center"><?php echo $solution_title; ?>
		    </div>
		<?php
			if( have_rows('solution_blocks') ):
			    while ( have_rows('solution_blocks') ) : the_row();
					$background_image2=get_sub_field('background_image2');
					$title=get_sub_field('title');
					$subtitle_1=get_sub_field('subtitle_1');
					$subtitle_2=get_sub_field('subtitle_2');
					$solution_description=get_sub_field('solution_description');
					$button_label=get_sub_field('button_label');
					$button_link=get_sub_field('button_link');
					?>
					<div class="col col--3">
			            <a href="<?php echo $button_link;?>">
			                <div class="solutions--item">
			                    <figure><img src="<?php echo $background_image2['url'];?>"></figure>
			                    <div class="solutions--item--first">
			                        <h2 class="main--title"><?php echo $title;?></h2>
			                        <h3 class="small--title"><?php echo $subtitle_1;?></h3>
			                        <h3 class="small--title"><?php echo $subtitle_2;?></h3>
			                    </div>
			                    <div class="corner"><img src="<?php echo get_template_directory_uri();?>/img/corner.png"></div>
			                    <div class="solutions--item--overlay">
			                        <h2 class="main--title"><?php echo $title;?></h2>
			                        <h3 class="small--title"><?php echo $subtitle_1;?></h3>
			                        <h3 class="small--title"><?php echo $subtitle_2;?></h3>
			                        <p><?php echo $solution_description;?></p>
			                        <span class="btn btn--main btn--main__reverse"><?php echo $button_label; ?></span>
			                    </div>
			                </div>
			            </a>
			        </div>
				<?php	
				endwhile;
			endif;
		 ?>
			</div>
		</section>
		<!-- SOLUTION BLOCK END -->
		<?php	
		endif;


		if( get_row_layout() == 'history_section' ):
			$caption_text=get_sub_field('caption_text');
			$background_image1=get_sub_field('background_image1');
			?>
			<div class="spacing__large"></div>
			<!-- SECONDARY BANNER START -->
			<section class="banner banner--secondary">
			    <div class="banner--image">
			        <figure><img src="<?php echo $background_image1['url']; ?>"></figure>
			    </div>
			    <div class="banner--content">
			        <?php echo get_sub_field('right_content');?>
			    </div>
			</section>
			<!-- SECONDARY BANNER END -->
		<?php		
		endif;
		?>
		

		<?php if( get_row_layout() == 'map_section' ){ ?>
		<!-- MAP SECTION START -->
		<section class="cms--map">
		<a href="<?php echo CONTACT_LINK; ?>">

		<div class="cms--map--holder">
		<div class="small--title small--title--topborder"><!-- <?php _e('Quatre adresses en Suisse','harsch'); ?> --><?php _e('Nos adresses en Suisse','harsch'); ?></div>
		<div class="map--pin animated animating animating--down"><span class="icon icon-pin"></span></div>
		<div class="map--cityName">
		<ul class="map--cityName--holder">
		 	<li class="map--cityName--city main--title"><?php _e('Genève','harsch'); ?></li>
		 	<li class="map--cityName--city main--title"><?php _e('Zurich','harsch'); ?></li>
		 	<li class="map--cityName--city main--title"><?php _e('Lausanne','harsch'); ?></li>
		 	<li class="map--cityName--city main--title"><?php _e('Bâle','harsch'); ?></li>
		</ul>
		</div>
		<figure><img src="<?php echo site_url(); ?>/wp-content/uploads/2016/11/map.jpg" /></figure></div>
		</a>
		</section>
			
		<?php }?>
		
		<?php
		if( get_row_layout() == '4_block_numbers' ):
			if( have_rows('4_block_number') ):
				?>
				<section class="fourBlocks text-center">
			<?php
			    while ( have_rows('4_block_number') ) : the_row();
					$block_background_image=get_sub_field('block_background_image');
					$text_number=get_sub_field('text_number');
					$link_text=get_sub_field('link_text');
					$link=get_sub_field('link');
					?>
				<!-- FOUR BLOCK START -->
			    <div class="col" style="background-image: url(<?php echo $block_background_image['url'];?>); ">
			      <?php if($link!=''){?>
			        <a href="<?php echo $link;?>" class="overlay--border animated fadeIn"></a>
			      <?php }?>
			        <div class="fourBlocks--holder animated animating animating--up">
			            <div class="">
			            	<svg width="220" height="70">
			                  <text x="110" y="60" text-anchor="middle" fill="none" stroke-width="1" stroke="white"><?php echo $text_number;?></text>
			                </svg>
			               
			            </div>
			            <div class="small--title small--title--topborder  small--title--white"><?php echo $link_text;?></div>
			        </div>
			    </div>
    
    		<?php	
				endwhile;
				?>
			</section>
			<!-- FOUR BLOCK END -->
			<?php	
			endif;
		endif;


		if( get_row_layout() == 'news_block' ){
		 ?>
		 <!-- TABBED SOCIAL START -->
		<div class="spacing__large theme__lightGrey"></div>
		<section class="socialFooter theme__lightGrey">
		    <div class="container container__medium">
		        <div class="socialSwiper swiper-container">
		            <div class="swiper-wrapper">
		                <div class="swiper-slide">
		                    <div class="feedBlock">

		                    <?php    $args = array(
					            'posts_per_page' => 3,
					            'orderby' => 'post_date',
					            'order' => 'DESC',
					            'post_type' => 'post',
					            'post_status' => 'publish',
					        		);
						        $readmore='readmore';
						        $recent_posts = new WP_Query($args);
						        while($recent_posts->have_posts())
						        {
						            $recent_posts->the_post();
						            ?>
				                     <div class="col col--3">
				                     <div class="post-image" style="background-image: url('<?php echo get_the_post_thumbnail_url( get_the_ID());  ?>'); ">

				                     	
				                     </div>
				                        <p class="grey nomargin"><?php echo get_the_date('d.m.y');?></p>
				                        <div class="small--title"><?php echo get_the_title();?></div>
				                        <!-- <p><?php echo get_excerpt(150);?></p> -->
				                        				                        <p><?php $content = strip_tags(get_the_content()); echo substr($content,0,150).'...';//echo get_excerpt(150);?></p>

				                        <a href="<?php echo get_the_permalink();?>" class="btn btn--normal"><?php _e('Lire','harsch');?></a>
				                    </div>
				         	 <?php  } 
				         	 wp_reset_query();
				         	 ?> 
		                       
		                    </div>
		                </div>
		                <div class="swiper-slide">
		                    <div class="linkedin text-center hidden-xsmall">
		                        <!--<script src="//platform.linkedin.com/in.js" type="text/javascript"></script> -->
                                        <script src="<?php echo get_template_directory_uri();?>/js/in.js" type="text/javascript"></script> 
		                        <script type="IN/CompanyInsider" data-id="555401"></script>
		                    </div>
		                    <div class="linkedin text-center visible-xsmall">
		                        <script type="IN/CompanyProfile" data-id="555401" data-format="inline" data-related="false" data-width="280px"></script>
		                    </div>
		                </div>
		                <div class="swiper-slide">
		                 <script>(function(d, s, id) {
		                          var js, fjs = d.getElementsByTagName(s)[0];
		                          if (d.getElementById(id)) return;
		                          js = d.createElement(s); js.id = id;
		                          js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
		                          fjs.parentNode.insertBefore(js, fjs);
		                        }(document, 'script', 'facebook-jssdk'));</script>
		                    <div class="facebook text-center hidden-xsmall">
		                        <div id="fb-root"></div>
		                        <!-- <script>(function(d, s, id) {
		                          var js, fjs = d.getElementsByTagName(s)[0];
		                          if (d.getElementById(id)) return;
		                          js = d.createElement(s); js.id = id;
		                          js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
		                          fjs.parentNode.insertBefore(js, fjs);
		                        }(document, 'script', 'facebook-jssdk'));</script> -->
		                        <div class="fb-page" data-href="https://www.facebook.com/Harsch-The-Art-of-Moving-Forward-1545925898758875/ " data-width="500px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
		                    </div>
		                    <div class="facebook text-center visible-xsmall">
		                        <div id="fb-root"></div>
		                        <!-- <script>(function(d, s, id) {
		                          var js, fjs = d.getElementsByTagName(s)[0];
		                          if (d.getElementById(id)) return;
		                          js = d.createElement(s); js.id = id;
		                          js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
		                          fjs.parentNode.insertBefore(js, fjs);
		                        }(document, 'script', 'facebook-jssdk'));</script> -->
		                        <div class="fb-page" data-href="https://www.facebook.com/Harsch-The-Art-of-Moving-Forward-1545925898758875/ " data-width="300px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
		                    </div>
		                    
		                </div>
		            </div>
		            <div class="swiper-pagination"></div>
		        </div>
		    </div>
		    
		</section>
		<div class="spacing__medium theme__lightGrey"></div>
		<!-- TABBED SOCIAL END -->
		
		<?php }


		if( get_row_layout() == '3_footers_icons' ){

			//die('test');
				$first_icon_text=get_sub_field('first_icon_text');
				$first_icon_url=get_sub_field('first_icon_url');
				$second_icon_text=get_sub_field('second_icon_text');
				$second_icon_url=get_sub_field('second_icon_url');
				$third_icon_text=get_sub_field('third_icon_text');
				$third_icon_url=get_sub_field('third_icon_url');
		 ?>

		<section>
		 <div class="spacing__medium"></div>
		 <div class="bigIcons container container__small">
            <?php //require get_template_directory() . '/template-parts/footer-icon.php';?>
                <a href="<?php echo $first_icon_url;?>" target="_blank" class="bigIcons--list text-center">
                    <span class="icon icon-bulb"></span>
                    <span class="btn btn--main"><?php echo $first_icon_text;?></span>
                </a>
                <a href="<?php echo $second_icon_url;?>" target="_blank" class="bigIcons--list text-center">
                    <span class="icon icon-letter"></span>
                    <span class="btn btn--main"><?php echo $second_icon_text;?></span>
                </a>
                <a href="<?php echo $third_icon_url;?>" target="_blank" class="bigIcons--list text-center">
                    <span class="icon icon-book"></span>
                    <span class="btn btn--main"><?php echo $third_icon_text;?></span>
                </a>
         </div>
		</section>
		
		<?php }  

	endwhile;
endif;

?>




<?php
get_footer();
?>