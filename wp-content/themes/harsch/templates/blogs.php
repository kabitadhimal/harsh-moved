<?php
/**
 * Template Name: Blogs page
 * The template for displaying all news posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package harsch
 */

get_header(); 
?>

<section class="news">
  <div class="theme__lightGrey news--category">
    <div class="container container__medium">
      <div class="main--title text-center"><?php _e('Actualités','harsch');?></div>
      
      <select class="customSelect" id="categoryid">
        <option value="all"><?php _e('All','harsch'); ?></option>
      <?php
                $categories = get_terms('category', 'orderby=count&hide_empty=0');
                foreach ($categories as $cat):
                  $selected = "";

                  if(!empty($_GET['cat']))
                  {
                    if($_GET['cat'] == $cat->slug)
                      $selected = "selected='selected'";
                  } 
                ?>
        <option value="<?php echo $cat->term_id; ?>" <?php echo "$selected"; ?>><?php echo $cat->name; ?></option>
                <?php
                endforeach;
      ?>
        
      </select>
      <div class="loading" style="display:none"> 
        <img src="<?php echo get_template_directory_uri().'/img/loading.svg'; ?>"/>
      </div>
    </div>
  </div>

  <div class="container container__medium">
    <div class="column" id="news_content">
            <?php
                $blog_args = array(
                        'post_type' => 'post',
                        'status' => 'publish',
                          );

                if(isset($_GET['cat']) && !empty($_GET['cat']))
                {
                  $blog_args['tax_query'] = array(
                                                array(
                                                  'taxonomy' => 'category',
                                                  'field'    => 'slug',
                                                  'terms'    => $_GET['cat'],
                                                ),
                                              );
                }

                query_posts($blog_args);
                if (have_posts()):
                while (have_posts()): the_post();
            ?>  

                  <div class="news--item">
                    <a href="<?php the_permalink();?>">
                      <figure>
                        <?php the_post_thumbnail('news_image');?>
                        <!-- <img src="http://placehold.it/400x400/ddacb7/fff"> -->
                      </figure>
                      <p class="grey nomargin"><?php echo get_the_date('d-m-Y');?></p>
                      <div class="small--title"><?php the_title(); ?></div>
                      <p><?php  $content = strip_tags(get_the_content()); echo substr($content,0,150); //echo get_excerpt(120);  ?></p>
                      <span class="btn btn--main"><?php _e('En savoir plus','harsch');?></span>
                    </a>
                    
                  </div>
            <?php
                endwhile;
                else:
                    _e('No news found.','harsch');
                endif;
                ?>  
      
    </div>
  </div>
</section>



<!--<?php echo get_template_directory_uri().'/img/giphy.gif'; ?>-->

<script>
$(document).ready(function(){

  /*$(document).on('click','.select-options > li',function(){

 alert("test");
  })*/

$("#categoryid").change(function(){
 // alert("asd");


 showValues();
})
/* $('.select-options').on('click',function(){
        alert("test");return false;
        
        
     });*/
});
function showValues() {
    var cat=$( "#categoryid" ).val();
    var $newsCat = $('.news--category');
    //var loading = "<?php echo get_template_directory_uri(); ?>/img/giphy.gif";
    //var $loader = $('<div class="loader"> <img src="'+loading+'" /> </div>);
   
         var url= "<?php echo get_template_directory_uri(); ?>/templates/ajaxnews.php";
         console.log(url);
            $.ajax({
                url:url,
                type: "GET",
                data: 'cat_id='+cat,
                beforeSend: function(){  $( ".loading" ).show();  },
                complete: function(){  $( ".loading" ).hide();    },
               // beforeSend: function() {
                  //$newsCat.find('.container__small').append($loader);
               // },
                success: function(msg) {
                  $('#news_content').html(msg);
                }
           });
   }
      //   change on click on select box
    


</script>

<?php
get_footer();