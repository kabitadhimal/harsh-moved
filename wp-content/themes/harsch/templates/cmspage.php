<?php
/**
 * Template Name: CMS page template 
 *
 * @package WordPress
 */

get_header(); 

?>

<?php
if( have_rows('cms') ):
	
    while ( have_rows('cms') ) : the_row();
		//echo get_row_layout();

		if( get_row_layout() == 'banner_section' ):
			$banner_text=get_sub_field('banner_text');
			$banner_image=get_sub_field('banner_image');
			
		
			?>
		<!-- BANNER START -->
		<section class="banner banner__cms">
		    <div class="banner--image">
		        <figure>
		        <?php if($banner_image!=''){?>	
		        <img src="<?php echo $banner_image['url'];?>">
		        <?php } else{ ?>
		        <img src="<?php echo get_template_directory_uri();?>/img/cmsbg.png">
		        <?php } ?>
		        </figure>
		    </div>
		    <div class="banner--content">
		        <div class="container container__medium">
		            <h1 class="main--title main--title__white">
		            <?php echo $banner_text;
		            	?>
		            </h1>
		        </div>
		    </div>
		    <div class="banner--overlay">
		        <figure><img src="<?php echo get_template_directory_uri()?>/img/banner-bottom.png"></figure>
		    </div>
		    <div class="scroller">
		        <figure><img src="<?php echo get_template_directory_uri()?>/img/down.png"></figure>
		    </div>
		</section>
		<!-- BANNER END -->
			<?php
		endif;


		if( get_row_layout() == 'intro_section' ):
			$title=get_sub_field('title');
			$short_content=get_sub_field('short_content');
			$background_image=get_sub_field('background_image');
			?>
			<!-- INTRO START -->
			<section class="intro" style="background-image: url(<?php echo $background_image['url']; ?>);">
			    <div class="container container__small">
			        <?php if(!empty($title)): ?>
				        <h2 class="text-center"><?php echo $title; ?></h2>
				    <?php endif; ?>
			        <div class="col--single text-center">
			            <p><?php echo $short_content; ?></p>
			        </div>
			    </div>    
			</section>
			<!-- INTRO END -->
		<?php	
		endif;

		
		if( get_row_layout() == 'two_column_block' ):
			$two_column_title=get_sub_field('two_column_title');
			$column1=get_sub_field('column1');
			$column2=get_sub_field('column2');
			$background_images=get_sub_field('background_images');
			?>
			<!-- INTRO START -->
			<section class="intro" style="background-image: url(<?php if($background_images!=''){ echo $background_images['url']; }?>);">
			    <div class="container container__small">
			        <h2 class="text-center"><?php echo $two_column_title; ?></h2>
			        <div class="col col--2 text-right">
			            <p class="text-justify">
			                <?php echo $column1; ?>
			            </p>

			            <!-- <a href="javascript:;" class="btn btn--main">Text button</a> -->
			        </div>
			        <div class="col col--2 text-left">
			            <p>
			                <?php echo $column2; ?>
			            </p>
			            <!-- <a href="javascript:;" class="btn btn--main">Text button</a> -->
			        </div>
			    </div>    
			</section>
			<!-- INTRO END -->
		<?php
		endif;	


		if( get_row_layout() == 'spaces' ):

			$size = "";
			$size = get_sub_field('size');
			$color = get_sub_field('color');

			$color_style = "";
			if(!empty($color))
				$color_style = "theme__".$color;
			
			?>

			<div class="spacing__<?php echo $size; ?> <?php echo $color_style; ?>"></div>

		<?php
		endif;
		

		if( get_row_layout() == '23_image_and_13_text' ):
			$information_title=get_sub_field('information_title');
			$single_23_image=get_sub_field('23_single_image');
			$text_13_title=get_sub_field('13_text_title');
			$text_13_content=get_sub_field('13_text_content');
			
			?>
			<!-- INFORMATION START -->
			<section class="information">
			    <div class="container container__medium">
			        <div class="small--title small--title--topborder small--title__largespace text-center"><?php echo $information_title;?></div>
			        <div class="col--two-three">
			            <figure>
			            
			            <img src="<?php echo $single_23_image['url'];?>">
			            </figure>
			        </div>
			        <div class="col--two-one">
			            <h2><?php echo $text_13_title;?></h2>
			            <p><?php echo $text_13_content;?></p>
			        </div>
			    </div>
			</section>
			<!-- INFORMATION END -->			
		<?php	
		endif;


		if( get_row_layout() == 'big_image' ):
			$big_image=get_sub_field('image');
		//	echo "<pre>";
			//	print_r($big_image);
			if($big_image!=''){
		?>
			<!-- SECONDARY BANNER START -->
			<section class="banner banner--secondary">
			    <div class="banner--image">
			        <figure>
				         <img src="<?php echo $big_image['url'];?>" alt="<?php echo $big_image['alt']; ?>"> 
			        </figure>
			    </div>
			</section>
			<!-- SECONDARY BANNER END -->
		<?php
			}
		endif;


		if( get_row_layout() == '13_quote_and_23_image' ):
			$quote13=get_sub_field('quote13');
			$image23=get_sub_field('image123');
			?>
			<?php
			if(get_sub_field('section_color1') == 'white')
				{ 	$sec_class='' ;
				}
			 elseif (get_sub_field('section_color1') == 'grey') {
			 		$sec_class='theme__grey' ;
			 	}
			 elseif (get_sub_field('section_color1') == 'darkgrey'){ 
			 		$sec_class='theme__darkerGrey' ;
				}
			?>
			<!-- IMAGE WITH QUOTE START -->
			<section class="imageQuote">
			    <div class="imageQuote--quote <?php echo $sec_class;?>">
			        <p class="para__large animated animating animating--left"><?php echo $quote13;?></p>
			    </div>
			    <div class="imageQuote--image">
			        <figure>
			        
			        <img src="<?php echo $image23['url'];?>">
			        </figure>
			    </div>
			</section>
		<?php
		endif;
		

		if( get_row_layout() == '23_image_and_13_quote' ):
			$quote=get_sub_field('quote_13');
			$image=get_sub_field('image_23');
			?>
			<?php
			if(get_sub_field('section_color2') == 'white')
				{ 	$sec_class='' ;
				}
			 elseif (get_sub_field('section_color2') == 'grey') {
			 		$sec_class='theme__grey' ;
			 	}
			 elseif (get_sub_field('section_color2') == 'darkgrey'){ 
			 		$sec_class='theme__darkerGrey' ;
				}
			?>	
			<section class="imageQuote imageQuote--reverse">
			    <div class="imageQuote--quote <?php echo $sec_class;?>">
			        <p class="para__large animated animating animating--right"><?php echo $quote;?></p>
			    </div>
			    <div class="imageQuote--image">
			        <figure>
			        
			        <img src="<?php echo $image['url'];?>">
			        </figure>
			    </div>
			</section>
			<!-- IMAGE WITH QUOTE END -->
		<?php	
		endif;


		if( get_row_layout() == 'cms_content_with_title' ):
			$content_title=get_sub_field('content_title');
			$cms_content=get_sub_field('cms_content');
			?>
			<!-- CMS CONTENT START -->
			<!-- <div class="spacing__large"></div> -->
			<section>
			    <div class="container container__medium">
			        <h2><?php echo $content_title;?></h2>
			        <p class="text-justify"><?php echo $cms_content;?></p>
			        		        
			    </div>
			</section>
			<!-- CMS CONTENT END -->
		<?php	
		endif;


		if( get_row_layout() == 'image_slider' ):
			if( have_rows('slider_image') ):
				?>
			<!-- CMS SLIDER START -->
			<div class="spacing__medium"></div>
			<section class="slider slider__cms">
			    <div class="container container__medium">
			        <div class="cms--slider">
			<?php
		    while ( have_rows('slider_image') ) : the_row();
					$simage = get_sub_field('slider_image1');
					
			?>		
			
			         <!--  <div><img src="http://placehold.it/790x520/ddacb7/fff"></div> -->
			           <div><img src="<?php echo $simage['url']; ?>"></div>
			
			<?php
			endwhile; 
			?>
			        </div>
			    </div>
			</section>
			<!-- CMS SLIDER END -->

			<?php
			endif;
		endif;


		if( get_row_layout() == 'icon_accordian_section' ):  ?>
		<!-- TABBED CONTENT START -->
		
		<?php if(get_sub_field('section_color') == 'white')
				{ 	$sec_class='' ;
				}
			 elseif (get_sub_field('section_color') == 'grey') {
			 		$sec_class='theme__grey' ;
			 	}
			 elseif (get_sub_field('section_color') == 'darkgrey'){ 
			 		$sec_class='theme__darkGrey' ;
				}

			 	$accordian_section_title=get_sub_field('accordian_section_title');
			 	$section_short_description=get_sub_field('section_short_description');
		?>

		<div class="spacing__large <?php echo $sec_class;?>"></div>

		<?php if($accordian_section_title != '' || $section_short_description != ''){ ?>
		<section class="<?php echo $sec_class;?>" >
		    <div class="container container__medium">
		    <h2 class="main--title text-left">
		    <?php echo $accordian_section_title; ?>
		    </h2>
		    <div class="spacing__medium"></div>
		    <p><?php echo $section_short_description; ?></p>
		    <div class="spacing__medium"></div>
		    </div>
		</section>    
		<?php }?>
		<section class="cms--tabs <?php echo $sec_class;?>" >
		    <div class="container container__medium text-center">
		    <div class="tabs desktopView">	
			<?php
				$cur='current';	
				static $c=0;
			    $randomNum = rand(1,100);
			    
				if( have_rows('accordian_icons') ):
					$i=0;
			    while ( have_rows('accordian_icons') ) : the_row();
			    	$i++;
			    	$randomNum++;
					$icon_class = get_sub_field('icon_class');
					$icon_label = get_sub_field('icon_label');
				?>
				<div class="tab-link <?php //if($i==1) echo $cur;?>" data-tab="tab-<?php echo $randomNum;?>">
                	<div class="icons"><span class="<?php echo $icon_class;?>"></span></div>
                	<h3 class="small--title"><?php echo $icon_label;?></h3>
            	</div>

				<?php				
				endwhile;
				$c=$i;
				$i=0;
				endif;
				 ?>
			</div>

			<div class="tabs mobileView">
            <!-- CONTENT SECTION -->
			<?php
			
				$r = ($randomNum-$c);
				
				if( have_rows('accordian_icons') ):
					$j=0;
					
			    while ( have_rows('accordian_icons') ) : the_row();
					$j++;
					$r++;
					$icon_accordian_title=get_sub_field('icon_accordian_title');
					$accordian_introduction=get_sub_field('accordian_introduction');
					$icon_class = get_sub_field('icon_class');
					$icon_label = get_sub_field('icon_label');
				?>
				<div class="tab-link <?php //if($j==1) echo $cur;?>" data-tab="tab-<?php echo $r;?>">
                	<div class="icons"><span class="<?php echo $icon_class;?>"></span></div>
                	<h3 class="small--title"><?php echo $icon_label;?></h3>
            	</div>
            
            <div id="tab-<?php echo $r;?>" class="tab-content <?php //if($j==1) echo $cur;?> animated fadeIn text-left">
                <h3 class="small--title"><?php echo $icon_accordian_title;?></h3>
                
                <p><?php echo $accordian_introduction;?></p>
                <div class="cms--accordion">
                    <ul class="accordion">
   						<?php	
						if( have_rows('accordian_content') ):
				   			while ( have_rows('accordian_content') ) : the_row();
								$title = get_sub_field('title1');
								
							?>
							<li>
                           	 <a href="javascript:;" class="accordion-title small--title">
                           	 	<?php echo $title;?></a>
	                            <div class="accordion-content">
	                                <?php echo get_sub_field('content');
										   	?>
	                            </div>
                        	</li>
							<?php	
							endwhile;
							
						endif;	
						?>	
					</ul>	
				</div>	
			  </div>	
			  <?php 
				 endwhile;
				
				endif; 
				?>
			</div>
    	 </div>
	  </section>
	  <!-- TABBED CONTENT END -->
	 <?php
	 endif;
		?>
	
	<?php if( get_row_layout() == 'map_section1' ){ ?>

		<!-- MAP SECTION START -->
		<section class="cms--map">
		<a href="<?php echo CONTACT_LINK; ?>">

		<div class="cms--map--holder">
		<div class="small--title small--title--topborder"><!-- <?php _e('Quatre adresses en Suisse','harsch'); ?> --><?php _e('Nos adresses en Suisse','harsch'); ?></div>
		<div class="map--pin animated animating animating--down"><span class="icon icon-pin"></span></div>
		<div class="map--cityName">
		<ul class="map--cityName--holder">
		 	<li class="map--cityName--city main--title"><?php _e('Genève','harsch'); ?></li>
		 	<li class="map--cityName--city main--title"><?php _e('Zurich','harsch'); ?></li>
		 	<li class="map--cityName--city main--title"><?php _e('Lausanne','harsch'); ?></li>
		 	<li class="map--cityName--city main--title"><?php _e('Bâle','harsch'); ?></li>
		</ul>
		</div>
		<figure><img src="<?php home_url(); ?>/wp-content/uploads/2016/11/map.jpg" /></figure></div>
		</a>
		</section>
			
	<?php } ?>

		
		<?php
		if( get_row_layout() == '4_block_numbers' ):
			if( have_rows('4_block_number') ):
				?>
				<section class="fourBlocks text-center">
			<?php
			    while ( have_rows('4_block_number') ) : the_row();
					$block_background_image=get_sub_field('block_background_image');
					$text_number=get_sub_field('text_number');
					$link_text=get_sub_field('link_text');
					$link=get_sub_field('link');
					?>
				<!-- FOUR BLOCK START -->
			    <div class="col" style="background-image: url(<?php echo $block_background_image['url'];?>); ">
			    	<?php if($link!=''){?>
			        <a href="<?php echo $link;?>" class="overlay--border animated fadeIn"></a>
			        <?php } ?>
			        <div class="fourBlocks--holder animated animating animating--up">
			            <div class="">
			            	<svg width="220" height="70">
			                  <text x="110" y="60" text-anchor="middle" fill="none" stroke-width="1" stroke="white"><?php echo $text_number;?></text>
			                </svg>
			               
			            </div>
			            <div class="small--title small--title--topborder  small--title--white"><?php echo $link_text;?></div>
			        </div>

			    </div>
    
    		<?php	
				endwhile;
				?>
			</section>
			<!-- FOUR BLOCK END -->
			<?php	
			endif;
		endif;


		if( get_row_layout() == 'contact_section' ):
			$contact_profile_image=get_sub_field('contact_profile_image');
			$imageId=$contact_profile_image['id'];
			$imgurl= wp_get_attachment_image_src( $imageId, 'medium', false );
			$contact_title=get_sub_field('contact_title');
			$contact_form=get_sub_field('contact_form');
			$contact_form_shortcode = '[contact-form-7 id="'.$contact_form[0]->ID.'" title="'.$contact_form[0]->post_title.'"]';
			
			?>
			<!-- FORM START -->
			<div class="spacing__medium theme__lightGrey" id="form"></div>
			<section class="cms--forms theme__lightGrey">
			    <div class="container container__medium">
			        <div class="small--title small--title--topborder"><?php echo $contact_title;?></div>
			        <div class="cms--forms--holder">
			             <?php if(isset($imgurl[0]) && !empty($imgurl[0])): ?>
			            <div class="form--image hidden-xsmall">
			                <div class="form--image--holder">
			                    <figure><img src="<?php echo $imgurl[0];?>"></figure>
			                </div>
			            </div>
			        <?php endif; ?>
			            <div class="form--container">
			            <?php
			            /*if(ICL_LANGUAGE_CODE == 'fr'){ echo do_shortcode('[contact-form-7 id="397" title="contact form fr"]');
			            	}
			            elseif (ICL_LANGUAGE_CODE == 'en') {
			            	echo do_shortcode('[contact-form-7 id="396" title="Contact form en"]');
			            	}
			            elseif (ICL_LANGUAGE_CODE == 'de') {
			            	echo do_shortcode('[contact-form-7 id="397" title="contact form fr"]');
			            	}	*/
			            echo do_shortcode($contact_form_shortcode);

			            ?>
			            </div>
			        </div>
			    </div>
			</section>
			<div class="spacing__medium theme__lightGrey"></div>
			
			
			 <?php if(isset($_GET['subject'])): ?>
    			
    			<script>
    			    $(document).ready(function(){
    			        
    			        console.log("> <?php echo $_GET['subject']; ?>");
    			        
    			        $(".customSelect").val("> <?php echo $_GET['subject']; ?>");

    			    })
    			</script>
			
			<?php endif; ?>
			
			<!-- FORM END -->
		<?php endif;

		 if( get_row_layout() == '3_footers_icons' ){

				$first_icon_text=get_sub_field('first_icon_text');
				$first_icon_url=get_sub_field('first_icon_url');
				$second_icon_text=get_sub_field('second_icon_text');
				$second_icon_url=get_sub_field('second_icon_url');
				$third_icon_text=get_sub_field('third_icon_text');
				$third_icon_url=get_sub_field('third_icon_url');
		 ?>

		<section>
		 <div class="spacing__medium"></div>
		 <div class="bigIcons container container__small">
            <?php //require get_template_directory() . '/template-parts/footer-icon.php';?>
                <a href="<?php echo $first_icon_url;?>" target="_blank" class="bigIcons--list text-center">
                    <span class="icon icon-bulb"></span>
                    <span class="btn btn--main"><?php echo $first_icon_text;?></span>
                </a>
                <a href="<?php echo $second_icon_url;?>" target="_blank" class="bigIcons--list text-center">
                    <span class="icon icon-letter"></span>
                    <span class="btn btn--main"><?php echo $second_icon_text;?></span>
                </a>
                <a href="<?php echo $third_icon_url;?>" target="_blank" class="bigIcons--list text-center">
                    <span class="icon icon-book"></span>
                    <span class="btn btn--main"><?php echo $third_icon_text;?></span>
                </a>
         </div>
		</section>
		
		<?php } 


	endwhile;
endif;

?>

<?php
get_footer();
?>