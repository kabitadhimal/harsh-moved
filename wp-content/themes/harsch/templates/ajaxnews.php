<?php 
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
//require_once('../../../../wp-load.php');
global $wp_query;
global $post;
//echo "ajax";

$sub_id = $_GET['cat_id'];

?>
 <div class="column" id="news_content">
	<?php 
		  if($sub_id!='' && $sub_id!='all'){
        $tax_query[] = array(
           'taxonomy' => 'category',
           'field' => 'term_id',
           'terms' => $sub_id,
           'include_children' => true,
             );
      }
			$newsquery = array(
            'post_type'      => array('post'),
            'post_status'    => 'publish',
            'tax_query'      => $tax_query,
            'orderby'        => 'date',
            'order'          => 'desc',
            
			);

			$new = new WP_Query($newsquery);
			//echo '<pre>';
			//print_r($new);
			if($new->have_posts()):
		    while ($new->have_posts()) : $new->the_post();
			?>
        		<div class="news--item">
                     <a href="<?php the_permalink();?>">
                      <figure>
                        <?php the_post_thumbnail('news_image');?>
                        <!-- <img src="http://placehold.it/400x400/ddacb7/fff"> -->
                      </figure>
                      <p class="grey nomargin"><?php echo get_the_date('d-m-Y');?></p>
                      <div class="small--title"><?php the_title(); ?></div>
                      <p><?php echo get_excerpt(120); ?></p>
                      <span class="btn btn--main"><?php _e('Read more','harsch');?></span>
                      </a>
            </div>
		<?php 
		 endwhile;
		 wp_reset_postdata();
		 else:
      _e('No news found in this category.','harsch');
     endif;
		?>
		
    </div>
