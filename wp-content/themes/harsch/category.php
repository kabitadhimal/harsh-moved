<?php
/**
 * 
 * The template for displaying all news posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package harsch
 */

get_header(); 
?>
<div class="spacing__medium"></div>
<section class="news">
  <div class="theme__lightGrey news--category">
    <div class="container container__small select__category">
      <div class="main--title text-center"><?php _e('Actualities','harsch');?></div>
      <select class="customSelect">
      <?php
      $categories = get_terms('category', 'orderby=count&hide_empty=0');
                 foreach ($categories as $cat):

                ?>
        <option value="<?php echo get_term_link($cat) ?>"><?php echo $cat->name; ?></option>
                    
                <?php
                  endforeach;
      ?>
        
      </select>
    </div>
  </div>

  <div class="container container__small">
    <div class="column">
            <?php
               
                if (have_posts()):
                while (have_posts()): the_post();
            ?>  

                  <div class="news--item">
                     
                      <figure>
                        <?php the_post_thumbnail('news_image');?>
                        <!-- <img src="http://placehold.it/400x400/ddacb7/fff"> -->
                      </figure>
                      <p class="grey nomargin"><?php echo get_the_date('d-m-Y');?></p>
                      <div class="small--title"><?php the_title(); ?></div>
                      <p><?php echo get_excerpt(120); ?></p>
                      <a href="<?php the_permalink();?>" class="btn btn--normal"><?php _e('Read more','harsch');?></a>
                    
                  </div>
            <?php
                endwhile;
                else:
                    _e('No news found in this category.','harsch');
                endif;
                ?>  
      
    </div>
  </div>
</section>

<?php
get_footer();