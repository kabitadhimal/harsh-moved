<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package harsch
 */

get_header(); ?>

<section class="news-detail">
  <div class="backLink theme__lightGrey">
  <div class="container container__medium">
  <?php if($_SERVER['HTTP_REFERER']!=''){
  	$link= $_SERVER['HTTP_REFERER'];
  	}else{
  	$link= home_url().'/news/';
  		}?>
  		<a href="<?php echo $link;?>">< <?php _e('Back','harsch'); ?></a></div>
  </div>
  <div class="container container__medium">
  <?php
		while ( have_posts() ) : the_post();
		?>
      <div class="column">
        <div class="header-title">
          <div class="main--title"><?php the_title(); ?></div>
          <p class="grey nomargin"><?php _e('posted on','harsch'); ?> <a><?php echo get_the_date('F'); ?></a></p>
        </div>
        <figure>
        <?php the_post_thumbnail('full');?>
          <!-- <img src="http://placehold.it/1250x400/ddacb7/fff"> -->
        </figure>
        <p><?php the_content(); ?></p>
        
      </div>
      <?php
      	endwhile;
      	?>
    </div>
</section>
<div class="spacing__medium"></div>

<?php
get_footer();
